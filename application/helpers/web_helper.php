<?php

function informasi($danger,$text){
  return '<div class="alert alert-'.$danger.'" role="alert">'.$text.'</div>';
}

function safe_echo_html($string){
  return trim(strip_tags(htmlspecialchars($string, ENT_QUOTES)));
}

function safe_echo_input($string='')
{
  return trim(preg_replace('/\s+/',' ', htmlspecialchars($string, ENT_QUOTES)));
}

function toRupiah($num){
  return "Rp <span class='rp-harga'>".number_format($num,0,'','.')."</span>";
}

function bulan($num){
  switch($num){
    case 1 : {
      return 'Januari';
      break;
    }
    case 2 : {
      return 'Februari';
      break;
    }
    case 3 : {
      return 'Maret';
      break;
    }
    case 4 : {
      return 'April';
      break;
    }
    case 5 : {
      return 'Mei';
      break;
    }
    case 6 : {
      return 'Juni';
      break;
    }
    case 7 : {
      return 'Juli';
      break;
    }
    case 8 : {
      return 'Agustus';
      break;
    }
    case 9 : {
      return 'September';
      break;
    }
    case 10 : {
      return 'Oktober';
      break;
    }
    case 11 : {
      return 'November';
      break;
    }
    case 12 : {
      return 'Desember';
      break;
    }
  }
}

function get_client_ip() {
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
  else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if(getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
  else if(getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if(getenv('HTTP_FORWARDED'))
     $ipaddress = getenv('HTTP_FORWARDED');
  else if(getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
  else
      $ipaddress = '127.0.0.1';
  return $ipaddress;
}

function slug($string){
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}

function kembali($str = ''){
  $ret  = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $str;
  redirect($ret);
}
