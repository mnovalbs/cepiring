<?php
  $config['pagination']['use_page_numbers'] = true;
  $config['pagination']['full_tag_open']    = '<nav class="paginat" aria-label="Page navigation"><ul class="pagination">';
  $config['pagination']['full_tag_close']   = '</ul></nav>';
  $config['pagination']['first_link']       = 'Pertama';
  $config['pagination']['last_link']        = 'Terakhir';
  $config['pagination']['num_tag_close']    = '</li>';
  $config['pagination']['cur_tag_open']     = '<li class="active"><span>';
  $config['pagination']['cur_tag_close']    = '</span></li>';
  $config['pagination']['prev_tag_open']    = $config['pagination']['next_tag_open']    = $config['pagination']['next_tag_open']  = $config['pagination']['num_tag_open']  = $config['pagination']['first_tag_open']  = $config['pagination']['last_tag_open']  = '<li>';
  $config['pagination']['prev_tag_close']   = $config['pagination']['next_tag_close']   = $config['pagination']['next_tag_close'] = $config['pagination']['num_tag_close'] = $config['pagination']['first_tag_close'] = $config['pagination']['last_tag_close'] ='</li>';
