<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['profile']             = 'warga/edit';
$route['produk']              = 'home/produk';
$route['produk/(:num)']       = 'home/produk/$1';
$route['desa-(:any)']         = 'home/desa/$1';
$route['produk-(:any)/(:any)/(:any)'] = 'home/getproduk/$1/$2/$3';
$route['desa-(:any)/produk']  = 'home/desa/$1';
$route['desa-(:any)/produk/(:num)'] = 'home/desa/$1/$2';
$route['default_controller']  = 'home';
$route['404_override']        = '';
$route['translate_uri_dashes'] = FALSE;
