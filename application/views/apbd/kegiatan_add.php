<div class='x_panel'>
  <div class='x_title'>
    <h2>Tambah Kegiatan</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->session->flashdata('pesan'); ?>
    <?= form_open('', array('class'=>'form-horizontal')); ?>
      <div class='form-group'>
        <label class='control-label col-md-4'>Kode : </label>
        <div class='col-md-2'>
          <input class='form-control' name='kk_kode' maxlength='4' required value='<?= set_value('kk_kode'); ?>'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-md-4'>Bidang : </label>
        <div class='col-md-5'>
          <select name='kk_bidang' class='form-control' required>
            <option value=''>Pilih Bidang</option>
            {bidang}
              <option value='{kb_id}'>{kb_nama}</option>
            {/bidang}
          </select>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-md-4'>Nama Kegiatan : </label>
        <div class='col-md-8'>
          <input class='form-control' name='kk_uraian' required maxlength='100' value='<?= set_value('kk_uraian'); ?>'/>
        </div>
      </div>

      <div class='ln_solid'></div>
      <div class='col-md-offset-4 col-md-8'>
        <button type='submit' class='btn btn-primary'><i class='fa fa-save'></i> Simpan</button>
        <a href='<?= base_url('kegiatan'); ?>' class='btn btn-danger'><i class='fa fa-remove'></i> Batal</a>
      </div>
    </form>
  </div>
</div>
