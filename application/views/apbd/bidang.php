<div class='x_panel'>
  <div class='x_title'>
    <h2>Bidang APBD</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_cotent'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->session->flashdata('pesan'); ?>
    <table class='table'>
      <thead>
        <tr>
          <th width='10'>No</th>
          <th>Nama Bidang</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;foreach($bidang as $bid){ ?>
          <tr>
            <td><?= $no++; ?></td>
            <td class='nama-bidang'><?= safe_echo_html($bid->kb_nama); ?></td>
            <td>
              <a href='#!' onclick='editBidang(this,<?= $bid->kb_id; ?>)' class='label label-primary'><i class='fa fa-pencil'></i> Edit</a>
              <a href='<?= base_url('apbd/bidang/delete/'.$bid->kb_id); ?>' onclick='return confirm("Hapus Bidang ini?")' class='label label-danger'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
        <?php } ?>
        <?= form_open(); ?>
          <tr>
            <td></td>
            <td><input class='form-control' required name='kk_nama' maxlength='100' placeholder='Nama bidang...'/></td>
            <td><button type='submit' name='tambah_bidang' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah</button></td>
          </tr>
        </form>
      </tbody>
    </table>
  </div>
</div>
