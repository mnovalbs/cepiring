<?php $kegiatan = $kegiatan['kegiatan']; ?>
<div class='x_panel'>
  <div class='x_title'>
    <h2>Tambah Kegiatan</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->session->flashdata('pesan'); ?>
    <?= form_open('', array('class'=>'form-horizontal')); ?>
      <div class='form-group'>
        <label class='control-label col-md-4'>Kode : </label>
        <div class='col-md-2'>
          <input class='form-control' name='kk_kode' maxlength='4' readonly value='<?= !empty(set_value('kk_kode')) ? set_value('kk_kode') : safe_echo_html($kegiatan->kk_kode); ?>'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-md-4'>Bidang : </label>
        <div class='col-md-5'>
          <select name='kk_bidang' class='form-control' required>
            <option value=''>Pilih Bidang</option>
            <?php foreach($bidang as $keg){ ?>
              <option value='<?= $keg->kb_id; ?>' <?= $keg->kb_id == $kegiatan->kk_bidang ? "selected" : ""; ?>><?= safe_echo_html($keg->kb_nama); ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-md-4'>Nama Kegiatan : </label>
        <div class='col-md-8'>
          <input class='form-control' name='kk_uraian' required maxlength='100' value='<?= !empty(set_value('kk_uraian')) ? set_value('kk_uraian') : safe_echo_html($kegiatan->kk_uraian); ?>'/>
        </div>
      </div>

      <div class='ln_solid'></div>
      <div class='col-md-offset-4 col-md-8'>
        <button type='submit' class='btn btn-primary'><i class='fa fa-save'></i> Simpan</button>
        <a href='<?= base_url('kegiatan'); ?>' class='btn btn-danger'><i class='fa fa-remove'></i> Batal</a>
      </div>
    </form>
  </div>
</div>
