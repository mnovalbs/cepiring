<a href='<?= base_url('apbd/kegiatan/add'); ?>' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Kegiatan</a>

<div class='x_panel'>
  <div class='x_title'>
    <h2>Kategori APBD</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <table class='table dtables'>
      <thead>
        <tr>
          <th>No</th>
          <th>Kode</th>
          <th>Bidang</th>
          <th>Uraian</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;foreach($kegiatan as $keg){ ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= safe_echo_html($keg->kk_kode); ?></td>
            <td><?= safe_echo_html($keg->kb_nama); ?></td>
            <td><?= safe_echo_html($keg->kk_uraian); ?></td>
            <td>
              <a href="<?= base_url('apbd/kegiatan/edit/'.$keg->kk_id); ?>" class='label label-primary'><i class='fa fa-pencil'></i> Edit</a>
              <a href='<?= base_url('apbd/kegiatan/delete/'.$keg->kk_id); ?>' onclick='return confirm("Hapus kegiatan ini?");' class='label label-danger'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
        <?php $no++;} ?>
      </tbody>
    </table>
  </div>
</div>
