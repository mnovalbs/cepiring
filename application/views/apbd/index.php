<div class='x_panel'>
  <div class='x_title'>
    <h2>APBD</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->session->flashdata('pesan'); ?>
    <table class='table'>
      <thead>
        <tr>
          <th width='10'>No</th>
          <th>Kategori</th>
          <th>Nama Anggaran</th>
          <th>Dibuat Oleh</th>
          <th>Dibuat Pada</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1;foreach($apbd as $ap){ ?>
          <tr data-id='<?= $ap->apbd_id; ?>'>
            <td><?= $no; ?></td>
            <td class='apbd_kategori'><?= $ap->apbd_kategori == 1 ? "<span data-id='1'>Bantuan Keuangan</span>" : "<span data-id='2'>PAD</span>"; ?></td>
            <td class='apbd_nama'><?= safe_echo_html($ap->apbd_nama); ?></td>
            <td><?= safe_echo_html($ap->user_name); ?></td>
            <td><?= safe_echo_html($ap->apbd_createdtime); ?></td>
            <td class='tombol'>
              <a href='#!' onclick='ubahAPBD(this)' class='label label-primary'><i class='fa fa-pencil'></i> Ubah</a>
              <a href='<?= base_url('apbd/delete/'.$ap->apbd_id); ?>' onclick='return confirm("Hapus anggaran ini?")' class='label label-danger'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
        <?php $no++;} ?>
        <?= form_open(); ?>
          <tr>
            <td></td>
            <td>
              <select name='apbd_kategori' class='form-control'>
                <option value='1'>Bantuan Keuangan</option>
                <option value='2'>PAD</option>
              </select>
            </td>
            <td><input autofocus placeholder='Nama anggaran' class='form-control' name='apbd_nama' required maxlength="40" value='<?= set_value('apbd_nama') ?>'/></td>
            <td colspan='2'><button type='submit' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah</button></td>
          </tr>
        </form>
      </tbody>
    </table>
  </div>
</div>
