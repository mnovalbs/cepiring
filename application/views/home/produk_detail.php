<?php $produk = $produk['produk']; ?>
<?php $user   = $user['user']; ?>
<div class='bg-top'></div>

<div class='container container-medium'>
  <div class='row'>
    <div class='col-md-8'>
      <div class='kotak-putih'>

        <div class='product-images'>
          <?php
            if( !empty($produk->produk_gambar) ){
              $produk_gambar  = explode(',', $produk->produk_gambar);
              ?>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <?php for($i=0;$i<count($produk_gambar);$i++){ ?>
                      <li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" class="<?= $i==0 ? "active" : ""; ?>"></li>
                    <?php } ?>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <?php foreach($produk_gambar as $k => $gbr){ ?>
                    <div class="item<?= $k == 0 ? " active" : ""; ?>">
                      <img src="<?= base_url('assets/file_desa/'.$produk->desa_key.'/'.$produk->user_key.'/'.$gbr); ?>">
                      <!-- <div class="carousel-caption">
                        ...
                      </div> -->
                    </div>
                    <?php } ?>
                  </div>
                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              <?php
            }else{
              echo "<img src='".base_url('assets/img/no-image-landscape.png')."'/>";
            }
          ?>
        </div>

        <div class='dalam'>
          <h2 class='product-title'><?= safe_echo_html($produk->produk_nama); ?></h2>
          <div class='product-detail'>
            <?= $produk->produk_deskripsi; ?>
          </div>
        </div>
      </div>
    </div>
    <div class='col-md-4'>
      <div class='kotak-putih'>
        <div class='dalam'>
          <div class='harga'><?= toRupiah($produk->produk_harga)."/".safe_echo_html($produk->produk_satuan); ?></div>
          <div class='detail-penjual'>
            <p><i class='fa fa-user'></i> <?= safe_echo_html($produk->user_name); ?></p>
            <p><i class='fa fa-map-marker'></i> <a href='<?= base_url('desa-'.$produk->desa_url); ?>'><?= safe_echo_html($produk->desa_name); ?></a></p>
            <p><i class='fa fa-phone'></i> <?= !empty($user->user_kontak) ? safe_echo_html($user->user_kontak) : "-"; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
