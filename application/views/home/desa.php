<?php
  $desa = $desa['desa'];
?>
<div class='bg-top' style='background-image:url("<?= !empty($desa->desa_thumbnail) ? base_url('assets/file_desa/'.$desa->desa_key.'/beauty/1350_250_'.$desa->desa_thumbnail) : base_url('assets/img/cepi.jpg'); ?>");'>
  <div class='container container-medium'>
    <div class='title'>
      <h2>Desa <?= safe_echo_html($desa->desa_name); ?></h2>
      <h3 style='font-size:20px;'><i class='fa fa-map-marker'></i> <?= safe_echo_html($desa->desa_address); ?></h3>
    </div>
  </div>
</div>

<div class='content-tengah'>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#umkm" aria-controls="home" role="tab" data-toggle="tab">UMKM</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Perangkat</a></li>
    <li role="presentation"><a href="#anggaran" aria-controls="messages" role="tab" data-toggle="tab">Anggaran</a></li>
  </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="umkm">
        <div class='container container-medium'>
          <div class='row'>
            <?= !empty($produk) ? "" : informasi('info', 'Belum ada UMKM dari Desa ini') ; ?>
            <?php
            foreach($produk as $prod){
              $thumb  = explode(',',$prod->produk_gambar);
            ?>
              <div class='col-md-4 col-sm-6 col-xs-12'>
                <div class='home-product'>
                  <div class='product-thumb'>
                    <div class='melayang'>
                      <p>
                        <a href='#!'>
                          <i class='fa fa-user'></i> <?= safe_echo_html($prod->user_name); ?>
                        </a>
                      </p>
                    </div>
                    <a href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>
                      <img src='<?= !empty($thumb[0]) ? base_url('assets/file_desa/'.$prod->desa_key.'/'.$prod->user_key).'/beauty/250_172_'.$thumb[0] : base_url('assets/img/no-image-landscape.png'); ?>'/>
                    </a>
                  </div>
                  <div class='product-detail'>
                    <h3>
                      <a href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>
                        <?= safe_echo_html( $prod->produk_nama ); ?>
                      </a>
                    </h3>
                    <a class='lokasi' href='<?= base_url('desa-'.$prod->desa_url); ?>'>
                      <i class='fa fa-map-marker'></i> <?= safe_echo_html($prod->desa_name); ?>
                    </a>
                    <div class='garis'></div>
                    <div class='row'>
                      <div class='col-md-8'>
                        <p class='product-price'>
                          <?= toRupiah($prod->produk_harga)."<small>/".safe_echo_html($prod->produk_satuan)."</small>"; ?>
                        </p>
                      </div>
                      <div class='col-md-4'>
                        <a class='tombol-detail' href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>Detail</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="profile">
        <div class='container container-medium'>
          <div class='table-putih'>
        <table class='table'>
          <thead>
            <tr>
              <th width='10'>No</th>
              <th>Jabatan</th>
              <th>Nama Perangkat</th>
              <th>No SK</th>
              <th>Tanggal SK</th>
              <th>Akhir Jabatan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1;foreach($perangkat as $per){ ?>
              <tr>
                <td><?= $no; ?></td>
                <td><?= safe_echo_html($per->desa_perangkat_jabatan); ?></td>
                <td><?= $per->user_name; ?></td>
                <td><?= safe_echo_html($per->desa_perangkat_sk_no); ?></td>
                <td><?= safe_echo_html($per->desa_perangkat_sk_tgl); ?></td>
                <td><?= safe_echo_html($per->desa_perangkat_akhir); ?></td>
              </tr>
            <?php $no++;} ?>
          </tbody>
        </table>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="anggaran">
        <div class='container-fluid'>
        <div class='form-group'>
          <select class='form-control' onchange='desa_select_tahun_anggaran(this)'>
            <?php
              for($i=date("Y")+1; $i>=1950; $i--){
                if( $i == date("Y") ){
                  echo "<option selected value='".$i."'>".$i."</option>";
                }else{
                  echo "<option value='".$i."'>".$i."</option>";
                }
              }
            ?>
          </select>
          <input value='<?= $desa->desa_id; ?>' name='desa_id' type='hidden'/>
        </div>
        <div class='table-ubah'>

          <div class='table-putih table-center'>
            <h3>Pendapatan</h3>
            <table class='table th-center'>
              <thead>
                <tr>
                  <?php $byk = 0;foreach($apbd_awal as $k => $ap){$byk += count($ap); ?>
                    <th colspan='<?= count($ap); ?>'>
                      <?= $k==1 ? "Bantuan Keuangan" : "PAD"; ?>
                    </th>
                  <?php } ?>
                </tr>
                <tr>
                  <?php foreach($apbd_awal as $k => $ap){ ?>
                    <?php foreach($ap as $a){ ?>
                      <th><?= safe_echo_html($a['apbd']); ?></th>
                    <?php } ?>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <tr class='angka-apbd'>
                  <?php $rp=0;foreach($apbd_awal as $k => $ap){ ?>
                    <?php foreach($ap as $a){$rp+=$a['res']; ?>
                      <td>
                        <?= toRupiah($a['res']); ?>
                      </td>
                    <?php } ?>
                  <?php } ?>
                </tr>
                <tr>
                  <td><b>Total : </b></td>
                  <td class='jumlah_apbd' style='font-weight:bold;text-align:center;' colspan='<?= ($byk-1); ?>'><?= toRupiah($rp); ?></td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class='table-putih'>
            <h3>Pengeluaran</h3>
            <table class='table'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Rek</th>
                  <th>Uraian Bidang dan Kegiatan</th>
                  <th>Uraian Proyek</th>
                  <?php foreach($apbd as $ab) {?>
                    <th width='100'><?= safe_echo_html($ab->apbd_nama); ?></th>
                  <?php } ?>
                  <th width='150'>Jumlah</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1;$jlhk=array();foreach($pemakaian as $pem) { ?>
                  <tr>
                    <td rowspan='<?= count($pem['pemakaian']); ?>'><?= $no; ?></td>
                    <td rowspan='<?= count($pem['pemakaian']); ?>'><?= safe_echo_html($pem['kode']); ?></td>
                    <td rowspan='<?= count($pem['pemakaian']); ?>'>
                        <?= safe_echo_html($pem['uraian']); ?>
                    </td>
                    <?php
                      $i=1;foreach($pem['pemakaian'] as $p){
                        $jlh=0;
                        echo $i>1 ? "<tr>" : "";
                        echo "<td>".safe_echo_html($p['uraian'])."</td>";
                        foreach($p['pemakaian'] as $k => $pp){
                          echo "<td class='".($pp > 0 ? "berwarna" : "")."'>".toRupiah($pp)."</td>";
                          $jlh += $pp;
                          if( empty($jlhk[$k]) ){
                            $jlhk[$k] = 0;
                          }
                          $jlhk[$k] += $pp;
                        }
                        echo "<td>".toRupiah($jlh)."</td>";
                        echo $i>1 ? "</tr>" : "";
                      $i++;}
                    ?>
                  </tr>
                <?php $no++;}$tot=0; ?>
                <tr>
                  <td colspan='4'>Jumlah</td>
                  <?php
                    foreach($jlhk as $jk){
                      $tot  += $jk;
                      echo "<td><b>".toRupiah($jk)."</b></td>";
                    }
                  ?>
                  <td><b><?= toRupiah($tot); ?></b></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>

  <?= $pagination; ?>

</div>
