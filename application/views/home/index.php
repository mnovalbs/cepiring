<div class='bg-top'>
  <div class='container container-medium'>
    <div class='title'>
      <h2>Kecamatan Cepiring</h2>
      <h3>Kabupaten Kendal</h3>
    </div>
  </div>
</div>

<div class='desa-wrapper'>
  <div class='container container-medium'>
    <div class='center-title'>
      <h3>DESA</h3>
    </div>
    <div class='row'>
      <?php $i=1;$no = 1;foreach($desa as $des){ ?>
        <div class='col-md-<?= ($no % 2 == 0) ? "7" : "5"; ?> col-sm-6'>
          <a href='<?= base_url('desa-'.$des->desa_url); ?>'>
            <div class='desas'>
              <div class='bg' style='background-image:url("<?= !empty($des->desa_thumbnail) ? ($no%2==0) ? base_url('assets/file_desa/'.$des->desa_key.'/beauty/495_280_'.$des->desa_thumbnail) : base_url('assets/file_desa/'.$des->desa_key.'/beauty/345_280_'.$des->desa_thumbnail) : base_url('assets/img/desa2.jpg'); ?>")'></div>
              <div class='nama-desa'>
                <div class='bawah'>
                  <span><?= safe_echo_html($des->desa_name); ?></span>
                  <p><?= !empty($des->desa_address) ? "<i class='fa fa-map-marker'></i> ".safe_echo_html($des->desa_address) : ""; ?></p>
                </div>
              </div>
            </div>
          </a>
        </div>
      <?php /*echo "<br/>i = ".$i." no : ".$no;*/$no += ($i % 2 == 0) ? 2 : 1;$i++;} ?>
    </div>
  </div>
</div>

<div class='container container-medium'>

  <div class='center-title'>
    <h3>UMKM</h3>
    <h4>Usaha Mikro Kecil Menengah</h4>
  </div>
  <div class='row'>
    <?php
    foreach($produk as $prod){
      $thumb  = explode(',',$prod->produk_gambar);
    ?>
      <div class='col-md-4 col-sm-6 col-xs-12'>
        <div class='home-product'>
          <div class='product-thumb'>
            <div class='melayang'>
              <p>
                <a href='#!'>
                  <i class='fa fa-user'></i> <?= safe_echo_html($prod->user_name); ?>
                </a>
              </p>
            </div>
            <a href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>
              <img src='<?= !empty($thumb[0]) ? base_url('assets/file_desa/'.$prod->desa_key.'/'.$prod->user_key).'/beauty/250_172_'.$thumb[0] : base_url('assets/img/no-image-landscape.png'); ?>'/>
            </a>
          </div>
          <div class='product-detail'>
            <h3>
              <a href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>
                <?= safe_echo_html( $prod->produk_nama ); ?>
              </a>
            </h3>
            <a class='lokasi' href='<?= base_url('desa-'.$prod->desa_url); ?>'>
              <i class='fa fa-map-marker'></i> <?= safe_echo_html($prod->desa_name); ?>
            </a>
            <div class='garis'></div>
            <div class='row'>
              <div class='col-md-8'>
                <p class='product-price'>
                  <?= toRupiah($prod->produk_harga)."<small>/".safe_echo_html($prod->produk_satuan)."</small>"; ?>
                </p>
              </div>
              <div class='col-md-4'>
                <a class='tombol-detail' href='<?= base_url('produk-'.$prod->desa_url.'/'.$prod->user_username.'/'.$prod->produk_url); ?>'>Detail</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  <?= $pagination; ?>
</div>
