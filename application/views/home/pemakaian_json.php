<div class='table-putih table-center'>
  <h3>Pendapatan</h3>
  <table class='table th-center'>
    <thead>
      <tr>
        <?php $byk = 0;foreach($apbd_awal as $k => $ap){$byk += count($ap); ?>
          <th colspan='<?= count($ap); ?>'>
            <?= $k==1 ? "Bantuan Keuangan" : "PAD"; ?>
          </th>
        <?php } ?>
      </tr>
      <tr>
        <?php foreach($apbd_awal as $k => $ap){ ?>
          <?php foreach($ap as $a){ ?>
            <th><?= safe_echo_html($a['apbd']); ?></th>
          <?php } ?>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <tr class='angka-apbd'>
        <?php $rp=0;foreach($apbd_awal as $k => $ap){ ?>
          <?php foreach($ap as $a){$rp+=$a['res']; ?>
            <td>
              <?= toRupiah($a['res']); ?>
            </td>
          <?php } ?>
        <?php } ?>
      </tr>
      <tr>
        <td><b>Total : </b></td>
        <td class='jumlah_apbd' style='font-weight:bold;text-align:center;' colspan='<?= ($byk-1); ?>'><?= toRupiah($rp); ?></td>
      </tr>
    </tbody>
  </table>
</div>

<div class='table-putih table-center'>
  <h3>Pengeluaran</h3>
  <table class='table'>
    <thead>
      <tr>
        <th>No</th>
        <th>Kode Rek</th>
        <th>Uraian Bidang dan Kegiatan</th>
        <th>Uraian Proyek</th>
        <?php foreach($apbd as $ab) {?>
          <th width='100'><?= safe_echo_html($ab->apbd_nama); ?></th>
        <?php } ?>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;$jlhk=array();foreach($pemakaian as $pem) { ?>
        <tr>
          <td rowspan='<?= count($pem['pemakaian']); ?>'><?= $no; ?></td>
          <td rowspan='<?= count($pem['pemakaian']); ?>'><?= safe_echo_html($pem['kode']); ?></td>
          <td rowspan='<?= count($pem['pemakaian']); ?>'>
            <?= safe_echo_html($pem['uraian']); ?>
          </td>
          <?php
            $i=1;foreach($pem['pemakaian'] as $p){
              $jlh=0;
              echo $i>1 ? "<tr>" : "";
              echo "<td>".safe_echo_html($p['uraian'])."</td>";
              foreach($p['pemakaian'] as $k => $pp){
                echo "<td class='".($pp > 0 ? "berwarna" : "")."'>".toRupiah($pp)."</td>";
                $jlh += $pp;
                if( empty($jlhk[$k]) ){
                  $jlhk[$k] = 0;
                }
                $jlhk[$k] += $pp;
              }
              echo "<td>".toRupiah($jlh)."</td>";
              echo $i>1 ? "</tr>" : "";
            $i++;}
          ?>
        </tr>
      <?php $no++;}$tot=0; ?>
      <tr>
        <td colspan='4'>Jumlah</td>
        <?php
          foreach($jlhk as $jk){
            $tot  += $jk;
            echo "<td><b>".toRupiah($jk)."</b></td>";
          }
        ?>
        <td><b><?= toRupiah($tot); ?></b></td>
      </tr>
    </tbody>
  </table>
</div>
