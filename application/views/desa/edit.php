<?php $desa = $desa['desa']; ?>
<?= form_open_multipart('', array('class'=>'form-horizontal')); ?>
<div class='x_panel'>
  <div class='x_title'>
    <h2>Edit Desa</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->config->item('pesan'); ?>
    <div class='form-group'>
      <label>Nama Desa * </label>
      <!-- <div class='col-md-8'> -->
        <input class='form-control' required name='desa_name' maxlength='40' value='<?= !empty(set_value('desa_name')) ? set_value('desa_name') : $desa->desa_name; ?>' placeholder='Nama desa'/>
      <!-- </div> -->
    </div>
    <div class='form-group'>
      <label class=''>Alamat Desa </label>
      <!-- <div class='col-md-8'> -->
        <textarea class='form-control' name='desa_address' maxlength='300' placeholder='Alamat desa'><?= !empty(set_value('desa_address')) ? set_value('desa_address') : $desa->desa_address; ?></textarea>
      <!-- </div> -->
    </div>
  </div>
</div>

<div class='x_panel'>
  <div class='x_title'>
    <h2>Thumbnail Desa</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <div class='desa-thumb' style='background-image:url("<?= !empty($desa->desa_thumbnail) ? base_url('assets/file_desa/'.$desa->desa_key.'/beauty/1350_250_'.$desa->desa_thumbnail) : base_url('assets/img/cepi.jpg'); ?>")'>
      <label for='change_thumb'><i class='fa fa-pencil'></i> ubah</label>
      <input id='change_thumb' onchange='gantiThumb(this)' type='file' name='desa_thumbnail' accept='image/*' style='display:none;'/>
    </div>
  </div>
</div>

<div class='x_panel'>
  <div class='x_title'>
    <h2>Perangkat Desa</h2>
    <div class='clearfix'></div>
  </div>

  <div class='x_content'>
    <div class='jabatan-box'>
      <?php if( empty($perangkat) ){ ?>
        <div class='form-group perangkat-input perangkat-pertama'>

          <div class='row'>
            <div class='col-md-11'>

              <div class='row'>
                <div class='col-md-5'>
                  <div class='form-group'>
                    <label>Nama Jabatan</label>
                    <input class='form-control' name='jabatan[0][name]' maxlength='40'/>
                  </div>
                </div>
                <div class='col-md-5'>
                  <div class='form-group'>
                    <label>Nama Perangkat</label>
                    <select class='form-control' name='jabatan[0][warga]'>
                      <option value=''>- Pilih Warga -</option>
                      {warga}
                      <option value='{user_id}'>{user_name}</option>
                      {/warga}
                    </select>
                  </div>
                </div>
                <div class='col-md-2'>
                  <div class='form-group'>
                    <label>Level</label>
                    <input class='form-control' name='jabatan[0][level]' type='number' min='1' max='50' value='1'/>
                  </div>
                </div>
              </div>

              <div class='row'>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Nomor SK</label>
                    <input class='form-control' maxlength="40" name='jabatan[0][sk_nomor]' />
                  </div>
                </div>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Tanggal SK</label>
                    <input class='form-control' type='date' name='jabatan[0][sk_tanggal]' />
                  </div>
                </div>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Tanggal Akhir Jabatan</label>
                    <input class='form-control' type='date' name='jabatan[0][akhir]' />
                  </div>
                </div>
              </div>

            </div>
            <div class='col-md-1'>
              <div class='form-group'>
                <label>Delete</label>
                <button type='button' onclick='perangkat_delete(this)' class='btn btn-danger'><i class='fa fa-trash'></i></button>
              </div>
            </div>
          </div>

        </div>
      <?php } ?>
      <?php $no = 0;foreach($perangkat as $per){ ?>
        <div class='form-group perangkat-input <?= $no != 0 ? "" : "perangkat-pertama"; ?>'>

          <div class='row'>
            <div class='col-md-11'>

              <div class='row'>
                <div class='col-md-5'>
                  <div class='form-group'>
                    <label>Nama Jabatan</label>
                    <input value='<?= $per->desa_perangkat_jabatan; ?>' class='form-control' name='jabatan[<?= $no; ?>][name]' maxlength='40'/>
                  </div>
                </div>
                <div class='col-md-5'>
                  <div class='form-group'>
                    <label>Nama Perangkat</label>
                    <select class='form-control' name='jabatan[<?= $no; ?>][warga]'>
                      <option value=''>- Pilih Warga -</option>
                      <?php
                        foreach($warga as $war){
                          if( $war->user_id == $per->user_id ){
                            echo "<option selected value='".$war->user_id."'>".safe_echo_html($war->user_name)."</option>";
                          }else{
                            echo "<option value='".$war->user_id."'>".safe_echo_html($war->user_name)."</option>";
                          }
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class='col-md-2'>
                  <div class='form-group'>
                    <label>Level</label>
                    <input class='form-control' name='jabatan[<?= $no; ?>][level]' type='number' min='1' max='50' value='<?= $per->desa_perangkat_level; ?>'/>
                  </div>
                </div>
              </div>

              <div class='row'>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Nomor SK</label>
                    <input class='form-control' maxlength="40" name='jabatan[<?= $no; ?>][sk_nomor]' value='<?= $per->desa_perangkat_sk_no; ?>'/>
                  </div>
                </div>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Tanggal SK</label>
                    <input class='form-control' type='date' name='jabatan[<?= $no; ?>][sk_tanggal]' value='<?= $per->desa_perangkat_sk_tgl; ?>'/>
                  </div>
                </div>
                <div class='col-md-4'>
                  <div class='form-group'>
                    <label>Tanggal Akhir Jabatan</label>
                    <input class='form-control' type='date' name='jabatan[<?= $no; ?>][akhir]' value='<?= $per->desa_perangkat_akhir; ?>'/>
                  </div>
                </div>
              </div>

            </div>
            <div class='col-md-1'>
              <label>Delete</label>
              <button type='button' onclick='perangkat_delete(this)' class='btn btn-danger'><i class='fa fa-trash'></i></button>
            </div>
          </div>
          <hr/>
        </div>
      <?php $no++;} ?>
    </div>
    <button type='button' class='btn btn-warning' onclick='tambah_perangkat()'><i class='fa fa-plus'></i> Tambah Perangkat</button>

    <div class='ln_solid'></div>
    <div class='form-group'>
      <button type='submit' class='btn btn-primary'><i class='fa fa-pencil'></i> Ubah</button>
      <a class='btn btn-danger' href='<?= base_url('desa'); ?>'><i class='fa fa-remove'></i> Batal</a>
    </div>
  </div>
</div>

</form>
