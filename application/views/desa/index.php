<a href='<?= base_url('desa/add'); ?>' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Desa</a>
<div class='x_panel'>
  <div class='x_content'>
    <?= $this->session->flashdata('pesan'); ?>
    <table class='table'>
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Desa</th>
          <th>Alamat</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1;foreach($desa as $des){ ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= safe_echo_html($des->desa_name); ?></td>
            <td><?= safe_echo_html($des->desa_address); ?></td>
            <td>
              <a class='label label-primary' href='<?= base_url('desa/edit/'.$des->desa_id); ?>'><i class='fa fa-pencil'></i> Ubah</a>
              <a class='label label-danger' href='<?= base_url('desa/delete/'.$des->desa_id); ?>' onclick='return confirm("Hapus Desa Ini?");'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
          <?php $no++;} ?>
        </tbody>
      </table>
  </div>
</div>
