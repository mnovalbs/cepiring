<h3 class='page-header'>Tambah Desa</h3>
<?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
<?= $this->config->item('pesan'); ?>
<?= form_open('', array('class'=>'form-horizontal')); ?>
  <div class='form-group'>
    <label class='control-label col-md-4'>Nama Desa : </label>
    <div class='col-md-8'>
      <input class='form-control' required name='desa_name' maxlength='40' value='<?= set_value('desa_name') ?>' placeholder='Nama desa'/>
    </div>
  </div>
  <div class='form-group'>
    <label class='control-label col-md-4'>Alamat Desa : </label>
    <div class='col-md-8'>
      <textarea class='form-control' name='desa_address' maxlength='300' placeholder='Alamat desa'><?= set_value('desa_address'); ?></textarea>
    </div>
  </div>
  <div class='col-md-8 col-md-offset-4'>
    <button type='submit' class='btn btn-primary'>Tambah</button>
  </div>
</form>
