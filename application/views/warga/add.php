<?= form_open('', array('class'=>'form-horizontal')); ?>
<div class='x_panel'>
  <?= $this->session->flashdata('pesan'); ?>
  <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
  <div class='x_title'>
    <h2>Tambah Warga</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Nama Warga * </label>
        <div class='col-sm-4 col-xs-12'>
          <input class='form-control' value='<?= set_value('user_name'); ?>' required name='user_name' maxlength='40'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Username * </label>
        <div class='col-sm-2 col-xs-12'>
          <input class='form-control' required name='user_username' value='<?= set_value('user_username'); ?>' minlength='4' maxlength='12' pattern='[a-zA-Z][a-zA-Z0-9_]{3,11}'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Password * </label>
        <div class='col-sm-4 col-xs-12'>
          <input class='form-control' required name='user_password' type='password' pattern='.{8,}'/>
        </div>
      </div>
      <?php if($this->config->item('me')->user_role_id == 1){ ?>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Desa * </label>
        <div class='col-sm-4 col-xs-12'>
          <select name='user_desa_id' class='form-control' required>
            {desa}
              <option value='{desa_id}'>{desa_name}</option>
            {/desa}
          </select>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Perangkat Kecamatan?</label>
        <div class='col-sm-9 col-xs-12'>
          <div class='inline-input'>
            <div class='radio'>
              <label><input class='flat' name='user_role_id' type='radio' <?= set_radio('user_role_id', '1'); ?> value='1'/> Ya</label>
              <label><input class='flat' name='user_role_id' type='radio' value='3' <?= set_radio('user_role_id', '3', true); ?>/> Tidak</label>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
  </div>
</div>
<div class='x_panel'>

  <div class='x_title'>
    <h2>Informasi Warga</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Nomor Telepon</label>
      <div class='col-sm-3 col-xs-12'>
        <input class='form-control' name='user_kontak' maxlength="16" value='<?= set_value('user_kontak'); ?>'/>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Tempat Tanggal Lahir</label>
      <div class='col-sm-3 col-xs-6'>
        <input class='form-control' name='user_lahir_tempat' maxlength='40' placeholder='Tempat Lahir' value='<?= set_value('user_lahir_tempat'); ?>'/>
      </div>
      <div class='col-sm-3 col-xs-6'>
        <input class='form-control' name='user_lahir_tanggal' type='date' placeholder='Tempat Lahir' value='<?= set_value('user_lahir_tanggal'); ?>'/>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Jenis Kelamin</label>
      <div class='col-sm-9 col-xs-12'>
        <div class='inline-input'>
          <div class='radio'>
            <label><input class='flat' name='user_jenis_kelamin' type='radio' <?= set_radio('user_jenis_kelamin', '1', true) ?> value='1'/> Pria</label>
            <label><input class='flat' name='user_jenis_kelamin' type='radio' value='2' <?= set_radio('user_jenis_kelamin', '2'); ?>/> Wanita</label>
          </div>
        </div>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Agama</label>
      <div class='col-sm-3 col-xs-12'>
        <select class='form-control' name='user_agama'>
          <option value='1' <?= set_select('user_agama', 1); ?>>Islam</option>
          <option value='2' <?= set_select('user_agama', 2); ?>>Kristen</option>
          <option value='3' <?= set_select('user_agama', 3); ?>>Katolik</option>
          <option value='4' <?= set_select('user_agama', 4); ?>>Hindu</option>
          <option value='5' <?= set_select('user_agama', 5); ?>>Budha</option>
        </select>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Sudah Menikah?</label>
      <div class='col-sm-3 col-xs-12'>
        <div class='inline-input'>
          <div class='radio'>
            <label><input class='flat' name='user_status_perkawinan' type='radio' <?= set_radio('user_status_perkawinan', '1', true); ?> value='1'/> Sudah</label>
            <label><input class='flat' name='user_status_perkawinan' type='radio' value='2' <?= set_radio('user_status_perkawinan', '2'); ?>/> Belum</label>
          </div>
        </div>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Pendidikan</label>
      <div class='col-sm-3 col-xs-12'>
        <input class='form-control' maxlength="20" name='user_pendidikan' placeholder='ex: SMA' value='<?= set_value('user_pendidikan'); ?>'/>
      </div>
    </div>
    <div class='ln_solid'></div>
    <div class='col-md-9 col-md-offset-3'>
      <button type='submit' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah</button>
      <a class='btn btn-danger' href='<?= base_url('warga'); ?>'><i class='fa fa-remove'></i> Batal</a>
    </div>
  </div>
</div>
</form>
