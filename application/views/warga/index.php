<a href='<?= base_url('warga/add'); ?>' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Warga</a>
<div class='x_panel'>
  <div class='x_title'>
    <h2>Warga</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= $this->session->flashdata('pesan'); ?>
    <table class='table'>
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Nama</th>
          <th>Desa</th>
          <th>Role</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1;foreach($warga as $war) { ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= safe_echo_html($war->user_username); ?></td>
            <td><?= safe_echo_html($war->user_name); ?></td>
            <td><?= safe_echo_html($war->desa_name); ?></td>
            <td><?= safe_echo_html($war->user_role_name); ?></td>
            <td>
              <a href='<?= base_url('warga/edit/'.$war->user_id); ?>' class='label label-primary'><i class='fa fa-pencil'></i> Ubah</a>
              <a href='<?= base_url('warga/delete/'.$war->user_id); ?>' onclick='return confirm("Hapus warga ini?")' class='label label-danger'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
        <?php $no++;} ?>
      </tbody>
    </table>
  </div>
</div>
