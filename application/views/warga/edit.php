<?php $warga = $warga['warga']; ?>
<?= form_open('', array('class'=>'form-horizontal')); ?>
<div class='x_panel'>
  <div class='x_title'>
    <h2>Edit Warga</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= $this->session->flashdata('pesan'); ?>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Nama Warga * </label>
        <div class='col-sm-4 col-xs-12'>
          <input class='form-control' value='<?= !empty(set_value('user_name')) ? set_value('user_name') : $warga->user_name; ?>' required name='user_name' maxlength='40'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Username * </label>
        <div class='col-sm-2 col-xs-12'>
          <input class='form-control' readonly name='user_username' value='<?= !empty(set_value('user_username')) ? set_value('user_username') : $warga->user_username; ?>' minlength='4' maxlength='12' pattern='[a-zA-Z][a-zA-Z0-9_]{3,11}'/>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Password</label>
        <div class='col-sm-4 col-xs-12'>
          <input class='form-control' name='user_password' type='password' pattern='.{8,}'/>
          <span>Password tidak perlu diisi, apabila tidak ingin diubah</span>
        </div>
      </div>
      <?php if($this->config->item('me')->user_role_id == 1){ ?>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Desa * </label>
        <div class='col-sm-4 col-xs-12'>
          <select name='user_desa_id' class='form-control' required>
            <?php
              foreach ($desa as $des) {
                if( $warga->user_desa_id == $des->desa_id ){
                  echo "<option value='".$des->desa_id."' selected>".safe_echo_html($des->desa_name)."</option>";
                }else{
                  echo "<option value='".$des->desa_id."'>".safe_echo_html($des->desa_name)."</option>";
                }
              }
            ?>
          </select>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-sm-3 col-xs-12'>Perangkat Kecamatan?</label>
        <div class='col-sm-9 col-xs-12'>
          <div class='inline-input'>
            <div class='radio'>
              <label><input class='flat' name='user_role_id' type='radio' <?= !empty(set_radio('user_role_id', '1')) ? set_radio('user_role_id', '1') : $warga->user_role_id == 1 ? "checked" : ""; ?> value='1'/> Ya</label>
              <label><input class='flat' name='user_role_id' type='radio' value='3' <?= !empty(set_radio('user_role_id', '3')) ? set_radio('user_role_id', '3') : $warga->user_role_id == 3 ? "checked" : ""; ?>/> Tidak</label>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
</div>
<div class='x_panel'>

  <div class='x_title'>
    <h2>Informasi Warga</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Nomor Telepon</label>
      <div class='col-sm-3 col-xs-12'>
        <input class='form-control' name='user_kontak' maxlength="16" value='<?= !empty(set_value('user_kontak')) ? set_value('user_kontak') : $warga->user_kontak; ?>'/>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Tempat Tanggal Lahir</label>
      <div class='col-sm-3 col-xs-6'>
        <input class='form-control' name='user_lahir_tempat' maxlength='40' placeholder='Tempat Lahir' value='<?= !empty(set_value('user_lahir_tempat')) ? set_value('user_lahir_tempat') : $warga->user_lahir_tempat; ?>'/>
      </div>
      <div class='col-sm-3 col-xs-6'>
        <input class='form-control' name='user_lahir_tanggal' type='date' placeholder='Tempat Lahir' value='<?= !empty(set_value('user_lahir_tanggal')) ? set_value('user_lahir_tanggal') : $warga->user_lahir_tanggal; ?>'/>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Jenis Kelamin</label>
      <div class='col-sm-9 col-xs-12'>
        <div class='inline-input'>
          <div class='radio'>
            <label><input class='flat' name='user_jenis_kelamin' type='radio' <?= !empty(set_radio('user_jenis_kelamin', '1')) ? set_radio('user_jenis_kelamin', '1') : $warga->user_jenis_kelamin == 1 ? "checked" : ""; ?> value='1'/> Pria</label>
            <label><input class='flat' name='user_jenis_kelamin' type='radio' value='2' <?= !empty(set_radio('user_jenis_kelamin', '2')) ? set_radio('user_jenis_kelamin', '2') : $warga->user_jenis_kelamin == 2 ? "checked" : ""; ?>/> Wanita</label>
          </div>
        </div>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Agama</label>
      <div class='col-sm-3 col-xs-12'>
        <select class='form-control' name='user_agama'>
          <option value='1' <?= !empty(set_select('user_agama',1)) ? set_select('user_agama', 1) : $warga->user_agama == 1 ? "selected" : ""; ?>>Islam</option>
          <option value='2' <?= !empty(set_select('user_agama',2)) ? set_select('user_agama', 2) : $warga->user_agama == 2 ? "selected" : ""; ?>>Kristen</option>
          <option value='3' <?= !empty(set_select('user_agama',3)) ? set_select('user_agama', 3) : $warga->user_agama == 3 ? "selected" : ""; ?>>Katolik</option>
          <option value='4' <?= !empty(set_select('user_agama',4)) ? set_select('user_agama', 4) : $warga->user_agama == 4 ? "selected" : ""; ?>>Hindu</option>
          <option value='5' <?= !empty(set_select('user_agama',5)) ? set_select('user_agama', 5) : $warga->user_agama == 5 ? "selected" : ""; ?>>Budha</option>
        </select>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Sudah Menikah?</label>
      <div class='col-sm-3 col-xs-12'>
        <div class='inline-input'>
          <div class='radio'>
            <label><input class='flat' name='user_status_perkawinan' type='radio' <?= !empty(set_radio('user_status_perkawinan', '1')) ? set_radio('user_status_perkawinan', '1') : $warga->user_status_perkawinan == 1 ? "checked" : ""; ?> value='1'/> Sudah</label>
            <label><input class='flat' name='user_status_perkawinan' type='radio' value='2' <?= !empty(set_radio('user_status_perkawinan', '2')) ? set_radio('user_status_perkawinan', '2') : $warga->user_status_perkawinan == 2 ? "checked" : ""; ?>/> Belum</label>
          </div>
        </div>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-3 col-xs-12'>Pendidikan</label>
      <div class='col-sm-3 col-xs-12'>
        <input class='form-control' maxlength="20" name='user_pendidikan' placeholder='ex: SMA' value='<?= !empty(set_value('user_pendidikan')) ? set_value('user_pendidikan') : $warga->user_pendidikan; ?>'/>
      </div>
    </div>
    <div class='ln_solid'></div>
    <div class='col-md-9 col-md-offset-3'>
      <button type='submit' class='btn btn-primary'><i class='fa fa-pencil'></i> Edit</button>
      <a class='btn btn-danger' href='<?= base_url('warga'); ?>'><i class='fa fa-remove'></i> Batal</a>
    </div>
  </div>
</div>
</form>
