<div class='x_panel'>
  <div class='x_title'>
    <h2>Tambah Produk</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= form_open_multipart(); ?>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>

    <div class='row'>

      <!-- Sebelah Kiri -->
      <div class='col-md-8'>
        <div class='form-group'>
          <label>Nama Produk</label>
          <input class='form-control' name='produk_nama' required maxlength='80' value='<?= set_value('produk_nama'); ?>'/>
        </div>

        <div class='form-group'>
          <label>Gambar Produk</label>
          <div class='thumbnail-box'>
            <div class='thumbnails'>
              <div class='kotak thumb-pertama'>
                <span class='hapus' onclick='hapusGambar(this)'><i class='fa fa-remove'></i></span>
                <input onchange='gantiGambar(this)' id='produkgambar0' type='file' name='produk_gambar[0]' accept='image/*'/>
                <label for='produkgambar0'><i class='fa fa-image'></i></label>
              </div>
            </div>
            <div class='add-box'>
              <div class='kotak'>
                <label><i class='fa fa-plus'></i></label>
              </div>
            </div>
          </div>
        </div>

        <div class='form-group'>
          <label>Deskripsi Produk</label>
          <textarea class='form-control tinymce' name='produk_deskripsi'><?= set_value('produk_deskripsi'); ?></textarea>
        </div>

      </div>

      <!-- Sebelah Kanan -->
      <div class='col-md-4'>
        <div class='form-group'>
          <label>Terlihat</label>
          <select class='form-control' name='produk_status' required>
            <option value='1' <?= set_select('produk_status', '1', true); ?>>Ya</option>
            <option value='2' <?= set_select('produk_status', '2'); ?>>Tidak</option>
          </select>
        </div>
        <div class='row'>
          <div class='col-md-8'>
            <div class='form-group'>
              <label>Harga</label>
              <div class='input-group'>
                <div class="input-group-addon">Rp</div>
                <input class='form-control' required min='0' type='number' name='produk_harga' value='<?= set_value('produk_harga'); ?>'/>
              </div>
            </div>
          </div>
          <div class='col-md-4'>
            <div class='form-group'>
              <label>Satuan</label>
              <div class='input-group'>
                <div class="input-group-addon">/</div>
                <input class='form-control' placeholder='kg' name='produk_satuan' value='<?= set_value('produk_satuan'); ?>'/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class='ln_solid'></div>

    <button type='submit' class='btn btn-success'>Tambah</button>
    <a class='btn btn-danger' href='<?= base_url('umkm'); ?>'><i class='fa fa-remove'></i> Batal</a>
    </form>
  </div>
</div>
