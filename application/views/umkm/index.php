<a href='<?= base_url('umkm/add'); ?>' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Produk</a>
<div class='x_panel'>
  <div class='x_title'>
    <h2>Daftar Produk</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= $this->session->flashdata('pesan'); ?>
    <table class='table'>
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Produk</th>
          <th>Harga Produk</th>
          <th>Status</th>
          <th>Pemilik</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;foreach($produk as $prod){ ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= safe_echo_html($prod->produk_nama); ?></td>
            <td><?= toRupiah($prod->produk_harga).'/'.safe_echo_html($prod->produk_satuan); ?></td>
            <td><?= $prod->produk_status == 1 ? "<label class='label label-primary'>Published</label>" : "<label class='label label-danger'>Hidden</label>"; ?></td>
            <td><?= safe_echo_html($prod->user_name); ?></td>
            <td>
              <a href='<?= base_url('umkm/edit/'.$prod->produk_id); ?>' class='label label-primary'><i class='fa fa-pencil'></i> Edit</a>
              <a onclick='return confirm("Hapus produk ini?");' href='<?= base_url('umkm/delete/'.$prod->produk_id); ?>' class='label label-danger'><i class='fa fa-remove'></i> Hapus</a>
            </td>
          </tr>
        <?php $no++;} ?>
      </tbody>
    </table>
  </div>
</div>
