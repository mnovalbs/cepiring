<table class='table'>
  <thead>
    <tr>
      <th>No</th>
      <th>Kode Rek</th>
      <th>Uraian Bidang dan Kegiatan</th>
      <th>Uraian Proyek</th>
      <?php foreach($apbd as $ab) {?>
        <th width='100'><?= safe_echo_html($ab->apbd_nama); ?></th>
      <?php } ?>
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1;$jlhk=array();foreach($pemakaian as $pem) { ?>
      <tr>
        <td rowspan='<?= count($pem['pemakaian']); ?>'><?= $no; ?></td>
        <td rowspan='<?= count($pem['pemakaian']); ?>'><?= safe_echo_html($pem['kode']); ?></td>
        <td rowspan='<?= count($pem['pemakaian']); ?>'>
          <a onclick='return confirm("Hapus pemakaian keuangan ini?");' href='<?= base_url('keuangan/pemakaian/delete/'.$pem['id']); ?>' style='color:#15ab21;'>
            <?= safe_echo_html($pem['uraian']); ?>
          </a>
        </td>
        <?php
          $i=1;foreach($pem['pemakaian'] as $p){
            $jlh=0;
            echo $i>1 ? "<tr>" : "";
            echo "<td>".safe_echo_html($p['uraian'])."</td>";
            foreach($p['pemakaian'] as $k => $pp){
              echo "<td class='".($pp > 0 ? "berwarna" : "")."'>".toRupiah($pp)."</td>";
              $jlh += $pp;
              if( empty($jlhk[$k]) ){
                $jlhk[$k] = 0;
              }
              $jlhk[$k] += $pp;
            }
            echo "<td>".toRupiah($jlh)."</td>";
            echo $i>1 ? "</tr>" : "";
          $i++;}
        ?>
      </tr>
    <?php $no++;}$tot=0; ?>
    <tr>
      <td colspan='4'>Jumlah</td>
      <?php
        foreach($jlhk as $jk){
          $tot  += $jk;
          echo "<td>".toRupiah($jk)."</td>";
        }
      ?>
      <td><?= toRupiah($tot); ?></td>
    </tr>
  </tbody>
</table>
