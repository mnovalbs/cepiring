<table class='table vertical-top'>
  <thead>
    <tr>
      <th width='10'>No</th>
      <th width='400'>Uraian</th>
      <th width='250'>Anggaran</th>
      <th>Bulan</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1;foreach($apbd as $ap){ ?>
      <tr>
        <td><?= $no; ?></td>
        <td><?= safe_echo_html($ap['apbd']); ?></td>
        <td>
          <div class='input-group'>
            <div class='input-group-addon'>Rp</div>
            <input class='form-control' name='apbd[<?= $ap['id'] ?>]' type='number' min='0' value='<?= $ap['res']->keuangan_anggaran; ?>'/>
          </div>
        </td>
        <td>-</td>
        <td>-</td>
      </tr>
    <?php $no++;} ?>
    <?php foreach($keuangan as $keu) { ?>
      <tr class='keuangan-row'>
        <td><?= $no; ?></td>
        <td>
          <div class='form-group'>
            <input class='form-control' name='keuangan[<?= $no; ?>][uraian]' maxlength='40' value='<?= $keu->keuangan_lain_uraian; ?>'/>
          </div>
        </td>
        <td>
          <div class='input-group'>
            <div class='input-group-addon'>Rp</div>
            <input class='form-control' name='keuangan[<?= $no; ?>][anggaran]' type='number' min='-1000000000' max='1000000000' value='<?= $keu->keuangan_lain_anggaran; ?>'/>
          </div>
        </td>
        <td>
          <div class='form-group'>
            <select class='form-control' name='keuangan[<?= $no; ?>][bulan]'>
              <option value=''>Bulan</option>
              <?php
                for($i=1;$i<=12;$i++){
                  echo "<option value='".$i."' ".( $keu->keuangan_lain_bulan == $i ? "selected" : "" ).">".bulan($i)."</option>";
                }
              ?>
            </select>
          </div>
        </td>
        <td>
          <div class='form-group'>
            <textarea placeholder='Keterangan...' name='keuangan[<?= $no; ?>][keterangan]' class='form-control' maxlength='300'><?= $keu->keuangan_lain_keterangan; ?></textarea>
          </div>
        </td>
      </tr>
    <?php $no++;} ?>
    <tr class='keuangan-row-pertama keuangan-row'>
      <td><?= $no; ?></td>
      <td>
        <div class='form-group'>
          <input class='form-control' name='keuangan[<?= $no; ?>][uraian]' maxlength='40' value='<?= set_value('keuangan['.$no.'][uraian]'); ?>'/>
        </div>
      </td>
      <td>
        <div class='input-group'>
          <div class='input-group-addon'>Rp</div>
          <input class='form-control' name='keuangan[<?= $no; ?>][anggaran]' type='number' min='-1000000000' max='1000000000' value='<?= set_value('keuangan['.$no.'][anggaran]'); ?>'/>
        </div>
      </td>
      <td>
        <div class='form-group'>
          <select class='form-control' name='keuangan[<?= $no; ?>][bulan]'>
            <option value=''>Bulan</option>
            <?php
              for($i=1;$i<=12;$i++){
                echo "<option value='".$i."'>".bulan($i)."</option>";
              }
            ?>
          </select>
        </div>
      </td>
      <td>
        <div class='form-group'>
          <textarea placeholder='Keterangan...' name='keuangan[<?= $no; ?>][keterangan]' class='form-control' maxlength='300'><?= set_value('keuangan['.$no.'][keterangan]'); ?></textarea>
        </div>
      </td>
    </tr>
    <!-- <tr class='tambah-row'>
      <td colspan='4' style='text-align:right;'>
        <button type='button' class='btn btn-info' onclick='tambahRowKeuangan()'><i class='fa fa-plus'></i> Tambah</button>
      </td>
    </tr> -->
  </tbody>
</table>
