
<div class='x_panel'>
  <div class='x_title'>
    <h2>Pendapatan Desa<!--<?= safe_echo_html($this->config->item('me')->desa_name); ?>--></h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= $this->session->flashdata('pesan'); ?>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= form_open('', array('class'=>'form-horizontal')); ?>

    <?php if( $this->config->item('me')->user_role_id == 1 ){ ?>
      <div class='form-group'>
        <label class='control-label col-md-4'>Desa : </label>
        <div class='col-md-4'>
          <select name='keuangan_desa' class='form-control' onchange='keuanganGantiTahun()'>
            <?php foreach($desa as $des){ ?>
              <option value='<?= $des->desa_id; ?>' <?= $desa_id == $des->desa_id ? "selected" : ""; ?>><?= safe_echo_html($des->desa_name); ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
    <?php } ?>

    <div class='form-group'>
      <label class='control-label col-md-4'>Tahun : </label>
      <div class='col-md-4'>
        <!-- <input class='form-control' name='keuangan_tahun' value='<?= date("Y"); ?>' type='number' min='1950'/> -->
        <select name='keuangan_tahun' class='form-control' onchange='keuanganGantiTahun()'>
          <?php for($i=date("Y")+1; $i>=1950; $i--){ ?>
            <option<?= $i == $tahun ? " selected" : ""; ?> value='<?= $i; ?>'><?= $i; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>


    <table class='table th-center'>
      <thead>
        <tr>
          <?php $byk = 0;foreach($apbd as $k => $ap){$byk += count($ap); ?>
            <th colspan='<?= count($ap); ?>'>
              <?= $k==1 ? "Bantuan Keuangan" : "PAD"; ?>
            </th>
          <?php } ?>
        </tr>
        <tr>
          <?php foreach($apbd as $k => $ap){ ?>
            <?php foreach($ap as $a){ ?>
              <th><?= safe_echo_html($a['apbd']); ?></th>
            <?php } ?>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <tr class='angka-apbd'>
          <?php $rp=0;foreach($apbd as $k => $ap){ ?>
            <?php foreach($ap as $a){$rp+=$a['res']; ?>
              <td>
                <!-- <div class='input-group'> -->
                  <!-- <div class='input-group-addon'>Rp</div> -->
                  <input class='form-control rp_apbd' name='apbd[<?= $a['id']; ?>]' type='number' value='<?= $a['res']; ?>'/>
                <!-- </div> -->
              </td>
            <?php } ?>
          <?php } ?>
        </tr>
        <tr>
          <td><b>Total : </b></td>
          <td class='jumlah_apbd' style='font-weight:bold;text-align:right;padding: 0 10px;' colspan='<?= ($byk-1); ?>'><?= toRupiah($rp); ?></td>
        </tr>
      </tbody>
    </table>



    <div class='ln_solid'></div>

    <div class='row'>
      <div class='col-md-6'>
        <button type='submit' class='btn btn-primary' name='submit_apbd'><i class='fa fa-save'></i> Simpan</button>
      </div>
    </div>
    <!-- <a href='<?= base_url('keuangan'); ?>'></a> -->

    </form>
  </div>
</div>

<!-- <div class='x_panel'>
  <div class='x_title'>
    <h2>Pemakaian</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>

  </div>
</div> -->
<!-- Dear Mr. Potter,
The ministry has received intelligence that at six twenty three this evening, you perform the patronous charm in the prsence
of a muggle as a clear violations of the decree for the reasonable retrictions of under age sorcery, you are hereby expelled
from Hogwarts School of Witchcraft and Wizardry. Hoping you are well, Mafalda Hopkirk. -->
