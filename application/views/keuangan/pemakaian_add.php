<?= form_open('', array('class'=>'form-horizontal')); ?>
<div class="x_panel">
  <div class='x_title'>
    <h2>Tambah Pemakaian</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <?= validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
    <?= $this->session->flashdata('pesan'); ?>
    <?php if( $this->config->item('me')->user_role_id == 1 ){ ?>
    <div class='form-group'>
      <label class='control-label col-md-2'>Desa : </label>
      <div class='col-md-4'>
        <select class='form-control' name='kp_desa' required>
          <?php foreach($desa as $des){ ?>
            <option value='<?= $des->desa_id; ?>' <?= set_select('kp_desa', $des->desa_id); ?>><?= safe_echo_html($des->desa_name); ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <?php } ?>
    <div class='form-group'>
      <label class='control-label col-md-2'>Tahun : </label>
      <div class='col-md-2'>
        <select class='form-control' name='kp_tahun' required>
          <?php for($i=date("Y")+1; $i>=1950; $i--){
            echo "<option value='".$i."' ".set_select('kp_tahun', $i).">".$i."</option>";
          }?>
        </select>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-md-2'>Bidang : </label>
      <div class='col-md-5'>
        <select class='form-control' onchange='keuanganChangeBidang(this)'>
          <option value=''>Pilih Bidang</option>
          {bidang}
            <option value='{kb_id}'>{kb_nama}</option>
          {/bidang}
        </select>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-md-2'>Kegiatan : </label>
      <div class='col-md-5'>
        <select class='form-control select2' name='kk_id' id='kegiatan_select' required>
          <option value=''>Pilih Bidang Dahulu</option>
        </select>
      </div>
    </div>
  </div>
</div>

<div class='x_panel'>
  <div class='x_title'>
    <h2>Kegiatan</h2>
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>

    <div class='kotak-kegiatan-tambah'>
      <div class='kegiatan-pertama'>
        <div class='form-group'>
          <label class='control-label col-md-2'>Uraian Proyek : </label>
          <div class='col-md-8'>
            <textarea maxlength="300" class='form-control' name='kegiatan[0][uraian]'></textarea>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-md-2'>Sumber Dana : </label>
          <div class='col-md-10 kotak-sumber'>
            <div class='row'>
              <div class='col-md-3'>
                <select class='form-control' name='kegiatan[0][sumber][0][apbd]'>
                  {apbd}
                  <option value='{apbd_id}'>{apbd_nama}</option>
                  {/apbd}
                </select>
              </div>
              <div class='col-md-4'>
                <div class='input-group'>
                  <div class='input-group-addon'>Rp</div>
                  <input class='form-control' type='number' min='0' value='0' name='kegiatan[0][sumber][0][jumlah]'/>
                </div>
              </div>
              <div class='col-md-2'>
                <button type='button' class='btn btn-success' onclick='tambahSumber(this, 0, 0)'><i class='fa fa-plus'></i> Sumber</button>
              </div>
              <div class='col-md-1 tombol-hapus'>

              </div>
            </div>
          </div>
        </div>
        <hr/>
      </div>
    </div>
    <button type='button' class='btn btn-default' onclick='tambah_kegiatan(this, 0)'><i class='fa fa-plus'></i> Tambah Kegiatan</button>

    <div class='ln_solid'></div>
    <button type='submit' class='btn btn-primary'><i class='fa fa-check'></i> Submit</button>
  </div>
</div>
</form>
