<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cepiring <?= !empty($title) ? " | ".$title : ""; ?></title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- DataTables -->
    <link href='<?= base_url('assets/datatables/DataTables-1.10.15/css/jquery.dataTables.min.css'); ?>' rel='stylesheet'/>
    <!-- Select2 -->
    <link href='<?= base_url('assets/select2/css/select2.min.css'); ?>' rel='stylesheet'/>

    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets/gentelella/build'); ?>/css/custom.min.css" rel="stylesheet">
    <link href='<?= base_url('assets/css/style.css?ver='.date("YmdHis")); ?>' rel='stylesheet'/>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?= base_url(); ?>" class="site_title"><i class="fa fa-cog"></i> <span>Cepiring Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?= base_url('assets/gentelella/images'); ?>/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?= $this->config->item('user_username'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <?php if( !empty($this->config->item('me')->desa_perangkat_level) || $this->config->item('me')->user_role_id == 1 ){ ?>
                  <li><a><i class="fa fa-home"></i> Master <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php if( $this->config->item('me')->user_role_id == 1 ){ ?>
                      <li><a href="<?= base_url('apbd'); ?>">APBD</a></li>
                      <li><a href='<?= base_url('apbd/bidang'); ?>'>Bidang APBD</a></li>
                      <li><a href="<?= base_url('apbd/kegiatan'); ?>">Kegiatan APBD</a></li>
                      <?php } ?>
                      <li><a href="<?= base_url('desa'); ?>">Desa</a></li>
                      <li><a href="<?= base_url('warga'); ?>">Warga</a></li>
                    </ul>
                  </li>
                  <?php } ?>
                  <li><a href='<?= base_url('umkm'); ?>'><i class="fa fa-shopping-cart"></i> UMKM</a></li>
                  <?php if( !empty($this->config->item('me')->desa_perangkat_level) ){ ?>
                    <li>
                      <a><i class='fa fa-money'></i> Keuangan <span class="fa fa-chevron-down"></span></a>
                      <ul class='nav child_menu'>
                        <li><a href='<?= base_url('keuangan'); ?>'>Pendapatan</a></li>
                        <li><a href='<?= base_url('keuangan/pemakaian'); ?>'>Pengeluaran</a></li>
                      </ul>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?= base_url('assets/gentelella/images'); ?>/img.jpg" alt=""><?= $this->config->item('user_username'); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?= base_url('profile'); ?>"> Profile</a></li>
                    <li><a href="<?= base_url('auth/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>


              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          {content}
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Flot/jquery.flot.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/moment/min/moment.min.js"></script>
    <script src="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- TinyMCE -->
    <script src='<?= base_url('assets/tinymce/tinymce.min.js'); ?>'></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= base_url('assets/gentelella/build'); ?>/js/custom.min.js"></script>
    <!-- DataTables -->
    <script src='<?= base_url('assets/datatables/DataTables-1.10.15/js/jquery.dataTables.min.js'); ?>'></script>
    <!-- Select2 -->
    <script src='<?= base_url('assets/select2/js/select2.min.js'); ?>'></script>
    <script>
      function base_url(str = ''){
        return "<?= base_url(); ?>"+str;
      }
    </script>
    <script src='<?= base_url('assets/js/global.js?ver='.date("YmdHis")); ?>'></script>

  </body>
</html>
