<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cepiring <?= !empty($title) ? " | ".$title : ""; ?></title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/gentelella/vendors'); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- DataTables -->
    <link href='<?= base_url('assets/datatables/datatables.min.css'); ?>' rel='stylesheet'/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
    <link href='<?= base_url('assets/css/style-home.css?ver='.date("YmdHis")); ?>' rel='stylesheet'/>
  </head>

  <body>
    <div class='nav-top'>
      <div class='container-fluid'>
        <div class='left'>
          <div class='title'>
            <a href='<?= base_url(); ?>'>
              <i class='fa fa-home'></i> Cepiring
            </a>
          </div>
        </div>
        <div class='right'>
          <div class='nav-menu'>
            <a href='<?= base_url('auth'); ?>'><i class='fa fa-sign-in'></i> Masuk</a>
          </div>
        </div>
      </div>
    </div>

    {content}


    <footer class='footer-wrapper'>
      <div class='container container-medium'>
        Copyright &copy; <?= date("Y"); ?> Kecamatan Cepiring | Dibangun oleh <a href='https://www.mnovalbs.id'>mnovalbs.id</a> - <a href='mailto:me@mnovalbs.id'>TIM 2 KKN UNDIP 2017</a>
      </div>
    </footer>

    <script src='<?= base_url('assets/js/jquery-3.1.1.min.js'); ?>'></script>
    <script src='<?= base_url('assets/datatables/datatables.min.js') ?>'></script>
    <script src='<?= base_url('assets/js/bootstrap.min.js'); ?>'></script>
    <script>function base_url(str=''){return "<?= base_url(); ?>"+str;}</script>
    <script src='<?= base_url('assets/js/global-home.js?ver='.date("YmdHis")); ?>'></script>
  </body>
</html>
