<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= "Kecamatan Cepiring" . (!empty($title) ? " : ".$title : ""); ?></title>
    <link href='<?= base_url('assets/css/bootstrap.min.css'); ?>' rel='stylesheet'/>
    <link href='<?= base_url('assets/css/style.css?ver='.date("YmdHis")); ?>' rel='stylesheet'/>
  </head>
  <body>
    <div class='container'>
      {content}
    </div>
  </body>
</html>
