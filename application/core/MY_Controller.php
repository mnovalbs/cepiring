<?php
  defined('BASEPATH') or die('No direct access accepted!');

  class MY_Controller extends CI_Controller{
    private $me;

    public function login_required_user(){
      $this->load->model('model_auth');

      $user = get_cookie('user');
      if( $user ){
        $user = $this->encryption->decrypt($user);
        $user = json_decode($user);
        $user = $this->model_auth->getUser($user->user_username);
        $this->me = $user;
        $this->config->set_item('me', $user);
        $this->config->set_item('user_username', $user->user_username);
        return $user;
      }else{
        redirect('auth');
      }
    }

    public function login_required_perangkat_desa(){
      $this->login_required_user();
      if( $this->me->user_role_id > 1 && empty($this->me->desa_perangkat_level)){
        delete_cookie('user');
        redirect('auth');
      }else{
        return $this->me;
      }
    }

    public function login_required_perangkat_kecamatan(){
      $this->login_required_user();
      if( $this->me->user_role_id > 1 ){
        delete_cookie('user');
        redirect('auth');
      }else{
        return $this->me;
      }
    }

    public function upload($file_name, $upload_path = FCPATH.'/assets/file_desa/', $types='gif|png|jpg', $max_size = 2048){
      $config['upload_path']    = $upload_path;
      $config['allowed_types']  = $types;
      $config['max_size']       = $max_size;
      $config['encrypt_name']   = true;
      $config['max_filename']   = 40;

      $this->load->library('upload', $config);

      if( !$this->upload->do_upload($file_name) ){
        // echo $this->upload->display_errors();
        return false;
      }else{
        return $this->upload->data();
      }
    }

    protected function beauty_crop_image($new_width, $new_height, $folder_path, $file_name){
      $folder_path  = substr($folder_path, -1) == '/' ? $folder_path : $folder_path.'/';

      if( !file_exists($folder_path.'rat/'.$new_width.'_'.$new_height.'_'.$file_name) ){
        unset($config);
        $this->load->library('image_lib');

        $config['image_library']  = 'gd2';
        $config['source_image']   = $folder_path.$file_name;

        list($width, $height, $type, $attr) = getimagesize($config['source_image']);

        $test_width   = $new_width / $width;
        $test_height  = $new_height / $height;

        if( $test_height > $test_width ){
          $height_resize  = $new_height;
          $width_resize   = (int)( $test_height * $width );
          $y_axis = 0;
          $x_axis = abs($new_width-$width_resize)/2;
        }else{
          $width_resize   = $new_width;
          $height_resize  = (int)( $test_width * $height );
          $x_axis = 0;
          $y_axis = abs($new_height-$height_resize)/2;
        }

        if( !file_exists($folder_path.'rat/') ){
          mkdir($folder_path.'rat/', 0777);
        }
        if( !file_exists($folder_path.'beauty/') ){
          mkdir($folder_path.'beauty/', 0777);
        }

        $config['width']      = $width_resize;
        $config['height']     = $height_resize;
        $config['maintain_ratio'] = FALSE;
        $config['new_image']  = $folder_path.'rat/'.$width_resize.'_'.$height_resize.'_'.$file_name;
        $new_image            = $config['new_image'];

        // echo "<pre style='margin-bottom:20px;'>";
        // echo "File Name : ".$file_name."<br/>";
        // echo "Width Asal : ".$width."<br/>Height Asal : ".$height;
        // echo "<br/>Width Resize : ".$width_resize."<br/>Height Resize : ".$height_resize;
        // echo "<br/>X_Axis : ".$x_axis."<br/>Y_Axis : ".$y_axis;
        // echo "<br/>New_Width : ".$new_width."<br/>New_Height : ".$new_height;
        // echo "</pre>";

        $this->image_lib->initialize($config);
        if(!file_exists($config['new_image'])){
          if($this->image_lib->resize()){

          }else{
            // echo $this->image_lib->display_errors();
          }
        }
        /* END Image Resize */

        /* START Image Cropping */
        unset($config);
        $config['image_library']  = 'gd2';
        $config['source_image']   = $new_image;
        $config['x_axis']     = $x_axis;
        $config['y_axis']     = $y_axis;
        $config['width']      = $new_width;
        $config['height']     = $new_height;
        $config['new_image']  = $folder_path.'beauty/'.$new_width.'_'.$new_height.'_'.$file_name;

        $this->image_lib->initialize($config);

        if ( $this->image_lib->crop()){
          return true;
        }else{
          return false;
        }
        /* END Image Cropping */
      }else{
        return true;
      }

    }

  }
