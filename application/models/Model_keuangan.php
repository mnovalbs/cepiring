<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_keuangan extends CI_Model{

    public function getListAPBD(){
      $this->db->select('*');
      $this->db->from('apbd');
      $this->db->order_by('apbd_kategori', 'ASC');
      // $this->db->join('keuangan b', 'b.apbd_id = a.apbd_id', 'full');
      // $this->db->where('b.desa_id', $desa_id);
      // $this->db->where('b.keuangan_tahun', $tahun);

      return $this->db->get()->result();
    }

    public function getKeuangan($apbd_id, $tahun, $desa_id){
      $this->db->select('*');
      $this->db->from('keuangan');
      $this->db->where('keuangan_tahun', $tahun);
      $this->db->where('apbd_id', $apbd_id);
      $this->db->where('desa_id', $desa_id);

      return $this->db->get()->row();
    }

    public function getListKeuanganLain($tahun, $desa_id){
      $this->db->select('*');
      $this->db->from('keuangan_lain');
      $this->db->where('desa_id', $desa_id);
      $this->db->where('keuangan_lain_tahun', $tahun);

      return $this->db->get()->result();
    }

    public function getListBidang(){
      $this->db->select('*');
      $this->db->from('keuangan_bidang');
      $this->db->order_by('kb_nama', 'ASC');

      return $this->db->get()->result();
    }

    public function getListAPBDbyKategori(){
      $this->db->select('apbd_kategori, GROUP_CONCAT(apbd_nama, ":", apbd_id) apbd');
      $this->db->from('apbd');
      $this->db->group_by('apbd_kategori');

      return $this->db->get()->result();
    }

    public function getKegiatanByBidang($bidang){
      $this->db->select('*');
      $this->db->from('keuangan_kegiatan');
      $this->db->where('kk_bidang', $bidang);
      $this->db->order_by('kk_kode');

      return $this->db->get()->result();
    }

    public function resetKeuangan($tahun, $desa_id){
      $this->db->where('desa_id', $desa_id);
      $this->db->where('keuangan_tahun', $tahun);
      $this->db->delete('keuangan');
    }

    public function getListPemakaian($desa_id, $tahun){
      $this->db->select('*');
      $this->db->from('keuangan_pemakaian a');
      $this->db->join('keuangan_kegiatan b', 'a.kk_id = b.kk_id');

      return $this->db->get()->result();
    }

    public function getListPemakaianDetail($desa_id, $tahun){
      $this->db->select('a.*, d.*, GROUP_CONCAT(b.apbd_id,":",c.apbd_nama,":",b.kpd_jumlah) pemakaian');
      $this->db->from('keuangan_pemakaian a');
      $this->db->join('keuangan_pemakaian_detail b', 'a.kp_id = b.kp_id', 'inner');
      $this->db->join('apbd c', 'c.apbd_id = b.apbd_id', 'inner');
      $this->db->join('keuangan_kegiatan d', 'd.kk_id = a.kk_id', 'inner');
      $this->db->group_by('a.kp_id');
      $this->db->where('a.desa_id', $desa_id);
      $this->db->where('a.kp_tahun', $tahun);

      return $this->db->get()->result();
    }

    public function resetKeuanganLain($tahun, $desa_id){
      $this->db->where('desa_id', $desa_id);
      $this->db->where('keuangan_lain_tahun', $tahun);
      $this->db->delete('keuangan_lain');
    }

    public function tambah($table_name, $data){
      $this->db->insert($table_name, $data);

      return $this->db->insert_id();
    }

    public function deletePemakaian($desa_id = NULL, $id){
      if( !empty($desa_id) ){
        $this->db->where('desa_id', $desa_id);
      }
      $this->db->where('kk_id', $id);
      $this->db->delete('keuangan_pemakaian');
    }

    public function getListDesa(){
      $this->db->select('*');
      $this->db->from('desa');
      $this->db->order_by('desa_name', 'ASC');

      return $this->db->get()->result();
    }

    // public function getKP($desa_id, $tahun){
    //   $this->db->select('*');
    //   $this->db->from
    // }

  }
