<?php
  class Model_apbd extends CI_Model{

    public function getListApbd(){
      $this->db->select('*');
      $this->db->from('apbd a');
      $this->db->join('user b', 'a.apbd_createdby = b.user_id', 'left');
      $this->db->order_by('a.apbd_kategori', 'ASC');
      $this->db->order_by('a.apbd_nama', 'ASC');

      return $this->db->get()->result();
    }

    public function getListBidang(){
      $this->db->select('*');
      $this->db->from('keuangan_bidang');
      $this->db->order_by('kb_nama', 'ASC');

      return $this->db->get()->result();
    }

    public function getListKegiatan(){
      $this->db->select('*');
      $this->db->from('keuangan_kegiatan a');
      $this->db->join('keuangan_bidang b', 'a.kk_bidang = b.kb_id');
      $this->db->order_by('a.kk_kode', 'ASC');

      return $this->db->get()->result();
    }

    public function delete_bidang($num){
      $this->db->where('kb_id', $num);
      $this->db->delete('keuangan_bidang');
    }

    public function delete_kegiatan($id){
      $this->db->where('kk_id', $id);
      $this->db->delete('keuangan_kegiatan');
    }

    public function edit_kegiatan($id){
      $this->db->select('*');
      $this->db->from('keuangan_kegiatan');
      $this->db->where('kk_id', $id);

      return $this->db->get()->row();
    }

    public function update_bidang($id, $data){
      $this->db->where('kb_id', $id);
      $this->db->update('keuangan_bidang', $data);
    }

    public function update_kegiatan($id, $data){
      $this->db->where('kk_id', $id);
      $this->db->update('keuangan_kegiatan', $data);
    }

    public function tambah($table_name, $data){
      $this->db->insert($table_name, $data);
    }

    public function edit($apbd_id, $data){
      $this->db->where('apbd_id', $apbd_id);
      $this->db->update('apbd', $data);
    }

    public function hapus($apbd_id){
      $this->db->where('apbd_id', $apbd_id);
      $this->db->delete('apbd');
    }

    public function isKegiatanAvailable($kode){
      $this->db->select('kk_kode');
      $this->db->from('keuangan_kegiatan');
      $this->db->where('kk_kode', $kode);

      return $this->db->get()->num_rows() != 0 ? false : true;
    }

    public function isApbdAvailable($apbd_nama){
      $this->db->select('*');
      $this->db->from('apbd');
      $this->db->where('apbd_nama', $apbd_nama);

      return $this->db->get()->num_rows() != 0 ? false : true;
    }

  }
