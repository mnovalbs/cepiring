<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_home extends CI_Model{

    public function getNumProduk($desa_id = NULL, $q = NULL){
      $this->db->select('a.*, c.user_key, c.user_username, c.user_name, d.*, GROUP_CONCAT(b.produk_gambar_url) produk_gambar');
      $this->db->from('produk a');
      if( !empty($desa_id) ){
        $this->db->where('d.desa_id', $desa_id);
      }
      if( !empty($q) ){
        $this->db->like('a.produk_nama', $q, 'both');
      }
      $this->db->where('a.produk_status', 1);
      $this->db->join('produk_gambar b', 'a.produk_id = b.produk_id', 'left');
      $this->db->join('user c', 'c.user_id = a.user_id');
      $this->db->join('desa d', 'd.desa_id = c.user_desa_id');
      $this->db->group_by('a.produk_id');
      $this->db->order_by('a.produk_id', 'DESC');

      return $this->db->get()->num_rows();
    }

    public function getListDesa(){
      $this->db->select('*');
      $this->db->from('desa');

      return $this->db->get()->result();
    }

    public function getUserInfo($user_id){
      $this->db->select('*');
      $this->db->from('user_info');
      $this->db->where('user_id', $user_id);

      return $this->db->get()->row();
    }

    public function getDetailProduk($desa_url, $user_username, $produk_url){
      $this->db->select('a.*, c.user_key, c.user_username, c.user_name, d.*, GROUP_CONCAT(b.produk_gambar_url) produk_gambar');
      $this->db->from('produk a');
      $this->db->where('a.produk_url', $produk_url);
      $this->db->where('d.desa_url', $desa_url);
      $this->db->where('c.user_username', $user_username);
      $this->db->where('a.produk_status', 1);
      $this->db->join('produk_gambar b', 'a.produk_id = b.produk_id', 'left');
      $this->db->join('user c', 'c.user_id = a.user_id');
      // $this->db->join('user_info e', 'e.user_id = c.user_id', 'left');
      $this->db->join('desa d', 'd.desa_id = c.user_desa_id');
      $this->db->group_by('a.produk_id');

      return $this->db->get()->row();
    }

    public function getListProduk($desa_id = NULL, $q = NULL, $start = 0, $limit = 12){
      $this->db->select('a.*, c.user_key, c.user_username, c.user_name, d.*, GROUP_CONCAT(b.produk_gambar_url) produk_gambar');
      $this->db->from('produk a');
      if( !empty($desa_id) ){
        $this->db->where('d.desa_id', $desa_id);
      }
      if( !empty($q) ){
        $this->db->like('a.produk_nama', $q, 'both');
      }
      $this->db->where('a.produk_status', 1);
      $this->db->join('produk_gambar b', 'a.produk_id = b.produk_id', 'left');
      $this->db->join('user c', 'c.user_id = a.user_id');
      $this->db->join('desa d', 'd.desa_id = c.user_desa_id');
      $this->db->group_by('a.produk_id');
      $this->db->order_by('a.produk_id', 'DESC');
      $this->db->limit($limit, $start);

      return $this->db->get()->result();
    }

    public function getDetailDesa($desa_url){
      $this->db->select('*');
      $this->db->from('desa');
      $this->db->where('desa_url', $desa_url);

      return $this->db->get()->row();
    }

    public function getListPerangkatDesa($desa_id){
      $this->db->select('a.*, b.user_name');
      $this->db->from('desa_perangkat a');
      $this->db->join('user b', 'a.user_id = b.user_id');
      $this->db->order_by('a.desa_perangkat_level', 'ASC');
      $this->db->where('a.desa_id', $desa_id);

      return $this->db->get()->result();
    }

  }
