<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_umkm extends CI_Model{

    public function add($table_name, $data){
      $this->db->insert($table_name, $data);
      return $this->db->insert_id();
    }

    public function isProductAvailable($user_id, $produk_url){
      $this->db->select('*');
      $this->db->from('produk');
      $this->db->where('user_id', $user_id);
      $this->db->where('produk_url', $produk_url);

      return $this->db->get()->num_rows() != 0 ? false : true;
    }

    public function getListProduk($user_id = NULL, $desa_id = NULL){
      $this->db->select('*');
      $this->db->from('produk a');
      $this->db->join('user b', 'a.user_id = b.user_id');
      $this->db->order_by('a.produk_id', 'DESC');
      // $this->db->join('desa c', 'c.desa_id = b.user_desa_id');

      if( !empty($user_id) ){
        $this->db->where('a.user_id', $user_id);
      }
      if( !empty($desa_id) ){
        $this->db->where('b.user_desa_id', $desa_id);
      }
      return $this->db->get()->result();
    }

    public function hapusGambar($gambar_id, $user_id = NULL, $desa_id = NULL){
      $this->db->where('produk_gambar_id', $gambar_id);
      $this->db->delete('produk_gambar');
    }

    public function ubahProduk($produk_id, $data){
      $this->db->where('produk_id', $produk_id);
      $this->db->update('produk', $data);
    }

    public function getDetailProduk($produk_id, $user_id = NULL, $desa_id = NULL){
      $this->db->select('*');
      $this->db->from('produk a');
      $this->db->join('user b', 'a.user_id = b.user_id');
      $this->db->where('a.produk_id', $produk_id);
      // $this->db->join('desa c', 'c.desa_id = b.user_desa_id');

      if( !empty($user_id) ){
        $this->db->where('a.user_id', $user_id);
      }
      if( !empty($desa_id) ){
        $this->db->where('b.user_desa_id', $desa_id);
      }
      return $this->db->get()->row();
    }

    public function getProdukGambar($produk_id){
      $this->db->select('*');
      $this->db->from('produk_gambar a');
      $this->db->join('produk b', 'a.produk_id = b.produk_id');
      $this->db->join('user c', 'b.user_id = c.user_id');
      $this->db->join('desa d', 'c.user_desa_id = d.desa_id');
      $this->db->where('a.produk_id', $produk_id);

      return $this->db->get()->result();
    }

  }
