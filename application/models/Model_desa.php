<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_desa extends CI_Model{

    public function tambah($table_name, $data){
      $this->db->insert($table_name, $data);
      return $this->db->insert_id();
    }

    public function resetPerangkat($desa_id){
      $this->db->where('desa_id', $desa_id);
      $this->db->delete('desa_perangkat');
    }

    public function getListPerangkat($desa_id){
      $this->db->select('*');
      $this->db->from('desa_perangkat a');
      // $this->db->join('user b', 'b.user_desa_id = a.desa_id', 'left');
      // $this->db->where('b.user_desa_id', $desa_id);
      $this->db->where('a.desa_id', $desa_id);
      // $this->db->group_by('a.desa_perangkat_id');
      $this->db->order_by('a.desa_perangkat_level', 'ASC');

      return $this->db->get()->result();
    }

    public function isWargaExists($user_id, $desa_id){
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_id', $user_id);
      $this->db->where('user_desa_id', $desa_id);

      return $this->db->get()->num_rows() != 0 ? true : false;
    }

    public function getListWarga($desa_id){
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_desa_id', $desa_id);

      return $this->db->get()->result();
    }

    public function isDesaExists($desa_id){
      $this->db->select('desa_id');
      $this->db->from('desa');
      $this->db->where('desa_id', $desa_id);

      return $this->db->get()->num_rows() != 0 ? true : false;
    }

    public function isDesaAvailable($desa_url){
      $this->db->select('desa_url');
      $this->db->from('desa');
      $this->db->where('desa_url', $desa_url);

      return $this->db->get()->num_rows() != 0 ? false : true;
    }

    public function getListDesa(){
      $this->db->select('*');
      $this->db->from('desa');

      return $this->db->get()->result();
    }

    public function edit($desa_id, $data){
      $this->db->where('desa_id', $desa_id);
      $this->db->update('desa', $data);
    }

    public function delete($num){
      $this->db->where('desa_id', $num);
      $this->db->delete('desa');
    }

    public function getDesa($num){
      $this->db->select('*');
      $this->db->from('desa');
      $this->db->where('desa_id', $num);

      return $this->db->get()->row();
    }

  }
