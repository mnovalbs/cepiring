<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_warga extends CI_Model{

    public function isWargaAvailable($username){
      $this->db->select('user_username');
      $this->db->from('user');
      $this->db->where('user_username', $username);

      return $this->db->get()->num_rows() != 0 ? false : true;
    }

    public function isWargaExists($user_id){
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_id', $user_id);

      return $this->db->get()->num_rows() != 0 ? true : false;
    }

    public function resetUserInfo($user_id){
      $this->db->where('user_id', $user_id);
      $this->db->delete('user_info');
    }

    public function getWarga($user_id, $desa_id = NULL){
      $this->db->select('*, a.user_id user_id');
      $this->db->from('user a');
      $this->db->join('user_info b', 'b.user_id = a.user_id', 'left');
      if( !empty($desa_id) ){
        $this->db->where('a.user_desa_id', $desa_id);
      }
      $this->db->where('a.user_id', $user_id);

      return $this->db->get()->row();
    }

    public function add($table_name, $data){
      $this->db->insert($table_name, $data);

      return $this->db->insert_id();
    }

    public function edit($user_id, $data){
      $this->db->where('user_id', $user_id);
      $this->db->update('user', $data);
    }

    public function getListWarga($exceptId = 0, $desa_id = NULL){
      $this->db->select('*');
      $this->db->from('user a');
      $this->db->join('desa b', 'a.user_desa_id = b.desa_id', 'left');
      $this->db->join('user_role c', 'a.user_role_id = c.user_role_id', 'left');
      $this->db->where('a.user_id !=', $exceptId);
      if( !empty($desa_id) ){
        $this->db->where('a.user_desa_id', $desa_id);
      }

      return $this->db->get()->result();
    }

  }
