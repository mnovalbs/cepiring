<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Model_auth extends CI_Model{

    public function getUser($username){
      $this->db->select('*, a.user_id user_id');
      $this->db->from('user a');
      $this->db->join('desa_perangkat b', 'a.user_id = b.user_id', 'left');
      $this->db->join('desa c', 'c.desa_id = a.user_desa_id', 'left');
      $this->db->where('a.user_username', $username);

      return $this->db->get()->row();
    }

  }
