<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Home extends MY_Controller{

    public function __construct(){
      parent::__construct();

      $this->load->model('model_home');
      $this->template->setTemplate('template/home');
    }

    public function index(){
      $data['title']  = "Selamat Datang!";
      $config = $this->config->item('pagination');
      $limit  = 9;

      $this->load->library('pagination');
      $config['base_url']   = base_url('/produk/');
      $config['total_rows'] = $this->model_home->getNumProduk();
      $config['per_page']   = $limit;

      $this->pagination->initialize($config);
      $data['desa']       = $this->model_home->getListDesa();
      $data['pagination'] = $this->pagination->create_links();
      $data['produk']     = $this->model_home->getListProduk(NULL, NULL, 0, $limit);
      $this->template->load('home/index', $data);
    }

    public function getproduk($desa_url = '', $user_username = '', $produk_url = ''){
      $produk = $this->model_home->getDetailProduk($desa_url, $user_username, $produk_url);
      if( empty($produk) ){
        redirect('');
      }
      $data['produk']['produk'] = $produk;
      $data['user']['user']     = $this->model_home->getUserInfo($produk->user_id);
      $data['title']  = "Produk : ".safe_echo_html($produk->produk_nama);
      $this->template->load('home/produk_detail', $data);
    }

    public function desa($url = '', $page = 1){
      $page   = (int)$page;
      $page   = $page < 1 ? 1 : $page;
      $limit  = 9;
      $start  = ($page - 1) * $limit;

      $desa = $this->model_home->getDetailDesa($url);
      if( empty($desa) ){
        redirect('');
      }

      $this->load->library('pagination');
      $this->load->model('model_keuangan');

      $config = $this->config->item('pagination');
      $config['base_url']   = base_url('desa-'.$desa->desa_url.'/produk/');
      $config['total_rows'] = $this->model_home->getNumProduk($desa->desa_id);
      $config['per_page']   = $limit;

      $this->pagination->initialize($config);

      $data['title']        = "Desa ".safe_echo_html($desa->desa_name);
      $data['desa']['desa'] = $desa;
      $data['pagination']   = $this->pagination->create_links();
      $data['perangkat']    = $this->model_home->getListPerangkatDesa($desa->desa_id);
      $data['produk']       = $this->model_home->getListProduk($desa->desa_id, NULL, $start, $limit);
      // $data['keuangan']     = $this->getAPBD(date("Y"), $desa->desa_id);
      $data['apbd']   = $this->model_keuangan->getListAPBD();

      $data['apbd_awal']= $this->getAPBDbyKategori(date("Y"), $desa->desa_id);
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain(date("Y"), $desa->desa_id);

      $data['pemakaian']  = $this->beautyPemakaianDetail($desa->desa_id, date("Y"));
      $this->template->load('home/desa',$data);
    }

    protected function getAPBDbyKategori($tahun = 0, $desa_id){
      $apbd = $this->getAPBD_Awal($tahun, $desa_id);
      $res  = array();
      foreach ($apbd as $k => $ap) {
        if(empty($res[$ap['kategori']])){
          $res[$ap['kategori']] = array();
        }
        array_push($res[$ap['kategori']], $ap);
      }
      return $res;
    }

    protected function getAPBD_Awal($tahun = 0, $desa_id){
      $tahun= (int)$tahun;
      $apbd = $this->model_keuangan->getListAPBD();
      $data = array();
      foreach($apbd as $k => $ap){
        $res  = $this->model_keuangan->getKeuangan($ap->apbd_id, $tahun, $desa_id);
        $data[$k]['id']     = $ap->apbd_id;
        $data[$k]['apbd']   = $ap->apbd_nama;
        $data[$k]['kategori'] = $ap->apbd_kategori;
        $data[$k]['res']    = !empty($res) ? $res->keuangan_anggaran : 0;
      }
      return $data;
    }

    public function jsonGetAnggaran($tahun = 0, $desa_id = 0){
      $data['apbd']     = $this->getAPBD($tahun, $desa_id);
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain($tahun, $desa_id);

      echo json_encode($data);
    }

    protected function getAPBD($tahun = 0, $desa_id){
      $this->load->model('model_keuangan');

      $tahun= (int)$tahun;
      $apbd = $this->model_keuangan->getListAPBD();
      $data = array();
      foreach($apbd as $k => $ap){
        $res  = $this->model_keuangan->getKeuangan($ap->apbd_id, $tahun, $desa_id);
        $data['apbd'][$k]['id']     = $ap->apbd_id;
        $data['apbd'][$k]['apbd']   = $ap->apbd_nama;
        // if( !empty($res) ){
        //   $data['apbd'][$k]['res']  = $res;
        // }else{
        //   $data['apbd'][$k]['res']->keuangan_anggaran = 0;
        // }
        $data['apbd'][$k]['res']    = !empty($res) ? $res : 0;
      }
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain($tahun, $desa_id);
      return $data;
    }

    public function produk($page = 1){
      $page   = (int)$page;
      $page   = $page < 1 ? 1 : $page;
      $limit  = 9;
      $start  = ($page - 1) * $limit;

      $this->load->library('pagination');

      $config = $this->config->item('pagination');
      $config['base_url']   = base_url('produk/');
      $config['total_rows'] = $this->model_home->getNumProduk();
      $config['per_page']   = $limit;

      $this->pagination->initialize($config);

      $data['title']      = "Selamat Datang!";
      $data['produk']     = $this->model_home->getListProduk(NULL,NULL,$start,$limit);
      $data['pagination'] = $this->pagination->create_links();
      $this->template->load('home/produk', $data);
    }

    public function beautyPemakaianDetail($desa_id, $tahun){
      $this->load->model('model_keuangan');
      $pemakaian  = $this->model_keuangan->getListPemakaianDetail($desa_id, $tahun);
      $apbd       = $this->model_keuangan->getListAPBD();
      $baru = array();
      foreach($pemakaian as $pem){
        if( empty( $baru[$pem->kk_kode] ) ){
          $baru[$pem->kk_kode]  = array('uraian'=>$pem->kk_uraian, 'id'=>$pem->kk_id, 'kode'=>$pem->kk_kode, 'pemakaian'=>array());
        }
        $pakai  = explode(',', $pem->pemakaian);
        $arp    = array();
        unset($a);
        foreach($pakai as $pak){
          $p  = explode(':', $pak);
          $a[$p[0]] = $p[2];
        }
        $b  = array();
        foreach ($apbd as $ad) {
          $b[$ad->apbd_id]  = !empty($a[$ad->apbd_id]) ? $a[$ad->apbd_id] : 0;
        }
        array_push($baru[$pem->kk_kode]['pemakaian'], array( 'uraian'=>$pem->kp_uraian, 'pemakaian'=>$b ));
      }

      return $baru;
    }

    public function pemakaian_json(){
      $tahun    = $this->input->post('tahun');
      $desa_id  = $this->input->post('desa_id');
      $this->load->model('model_keuangan');
      $data['apbd']   = $this->model_keuangan->getListAPBD();
      $data['pemakaian']  = $this->beautyPemakaianDetail($desa_id, $tahun);
      $data['apbd_awal']= $this->getAPBDbyKategori($tahun, $desa_id);
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain($tahun, $desa_id);
      $this->load->view('home/pemakaian_json', $data);
    }

  }
