<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Desa extends MY_Controller{
    private $me;

    public function __construct(){
      parent::__construct();
      $this->load->model('model_desa');
      $this->me = $this->login_required_perangkat_desa();
      $this->template->setTemplate('template/cepiring');
    }

    public function index(){
      if( $this->me->user_role_id != 1 ){
        $this->edit($this->me->desa_id);
      }else{
        $data['title']  = "Daftar Desa";
        $data['desa']   = $this->model_desa->getListDesa();
        $this->template->load('desa/index', $data);
      }
    }

    public function edit($num = 0){
      if( $num != $this->me->desa_id ){
        $this->login_required_perangkat_kecamatan();
      }
      $desa = $this->model_desa->getDesa($num);
      if( empty($desa) ){
        $this->session->set_flashdata('pesan', informasi('danger', 'Desa tidak ditemukan.'));
        redirect('desa');
      }

      $this->form_validation->set_rules('desa_name', 'nama desa', 'required|trim|max_length[40]');
      $this->form_validation->set_rules('desa_address', 'alamat desa', 'trim|max_length[300]');

      if( $this->form_validation->run() != false ){
        $upd['desa_name']     = $this->input->post('desa_name');
        $upd['desa_address']  = $this->input->post('desa_address');

        if( !empty($_FILES['desa_thumbnail']['tmp_name']) ){
          if( !file_exists(FCPATH.'assets/file_desa/'.$desa->desa_key.'/') ){
            mkdir(FCPATH.'assets/file_desa/'.$desa->desa_key.'/', 0777);
          }
          $desa_thumbnail = $this->upload('desa_thumbnail', FCPATH.'assets/file_desa/'.$desa->desa_key.'/');
          if( $desa_thumbnail != false ){
            $this->beauty_crop_image(1350, 250, FCPATH.'assets/file_desa/'.$desa->desa_key.'/', $desa_thumbnail['file_name']);
            $this->beauty_crop_image(345, 280, FCPATH.'assets/file_desa/'.$desa->desa_key.'/', $desa_thumbnail['file_name']);
            $this->beauty_crop_image(495, 280, FCPATH.'assets/file_desa/'.$desa->desa_key.'/', $desa_thumbnail['file_name']);
            $upd['desa_thumbnail']  = $desa_thumbnail['file_name'];
          }
        }

        $this->model_desa->edit($desa->desa_id, $upd);
        $this->model_desa->resetPerangkat($num);

        $perangkat_desa       = $this->input->post('jabatan');

        foreach ($perangkat_desa as $perangkat) {
          $level  = (int)$perangkat['level'];
          if( !empty(trim($perangkat['name'])) && $this->model_desa->isWargaExists($perangkat['warga'], $num) && $level <= 50 && $level >= 0 ){
            $per['desa_perangkat_jabatan']  = $perangkat['name'];
            $per['desa_perangkat_level']    = $level;
            $per['desa_perangkat_sk_no']    = $perangkat['sk_nomor'];
            $per['desa_perangkat_sk_tgl']   = $perangkat['sk_tanggal'];
            $per['desa_perangkat_akhir']    = $perangkat['akhir'];
            $per['desa_id']   = $num;
            $per['user_id']   = $perangkat['warga'];
            $this->model_desa->tambah('desa_perangkat', $per);
          }
        }

        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil mengubah desa'));
        redirect('desa');
      }

      $data['perangkat']    = $this->model_desa->getListPerangkat($num);
      $data['warga']        = $this->model_desa->getListWarga($num);
      $data['desa']['desa'] = $desa;
      $this->template->load('desa/edit', $data);
    }

    public function add(){
      $this->login_required_perangkat_kecamatan();
      $this->form_validation->set_rules('desa_name', 'nama desa', 'required|trim|max_length[40]');
      $this->form_validation->set_rules('desa_address', 'alamat desa', 'trim|max_length[300]');

      if( $this->form_validation->run() != false ){
        $ins['desa_name']     = $this->input->post('desa_name');
        $ins['desa_address']  = $this->input->post('desa_address');
        $ins['desa_url']      = slug($ins['desa_name']);
        $ins['desa_key']      = sha1( date("YmdHis").rand(1,100).rand(1,100).rand(1,100).rand(1,100).rand(1,100).rand(1,100) );
        $ins['desa_createdtime']  = date("Y-m-d H:i:s");
        $ins['desa_createdby']    = $this->me->user_id;

        $i = 2;
        while( !$this->model_desa->isDesaAvailable($ins['desa_url']) ){
          $ins['desa_url']  = slug($ins['desa_name']." - ".$i);
          $i++;
        }

        $desa_id = $this->model_desa->tambah('desa', $ins);
        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan desa baru'));
        redirect('desa/edit/'.$desa_id);
      }

      $data['title']  = "Tambah Desa";
      $this->template->load('desa/add', $data);
    }

    public function delete($num = 0){
      $this->model_desa->delete($num);
      $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menghapus desa'));
      redirect('desa');
    }
  }
