<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Dashboard extends MY_Controller{
    private $me;

    public function __construct(){
      parent::__construct();
      $this->me = $this->login_required_user();
      $this->template->setTemplate('template/cepiring');
    }

    public function index(){
      $data['title']    = "Dashboard";
      $this->template->load('dashboard/index', $data);
    }

  }
