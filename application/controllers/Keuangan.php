<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Keuangan extends MY_Controller{

    private $me;

    public function __construct(){
      parent::__construct();

      $this->me = $this->login_required_perangkat_desa();
      $this->template->setTemplate('template/cepiring');
      $this->load->model('model_keuangan');
    }

    public function index(){
      $this->form_validation->set_rules('keuangan_tahun', 'tahun', 'required');
      $desa_id  = $this->me->desa_id;

      if( $this->form_validation->run() != false ){
        $tahun    = $this->input->post('keuangan_tahun');
        $id_desa  = $this->input->post('keuangan_desa');

        if( $this->me->user_role_id == 1 && !empty($id_desa) ){
          $desa_id  = $id_desa;
        }

        $this->model_keuangan->resetKeuangan($tahun, $desa_id);
        $this->model_keuangan->resetKeuanganLain($tahun, $desa_id);

        foreach($_POST['apbd'] as $kApbd => $pApbd){
          $ins['apbd_id']   = $kApbd;
          $ins['desa_id']   = $this->me->desa_id;
          $ins['keuangan_tahun']    = $tahun;
          $ins['keuangan_anggaran'] = $pApbd;
          $ins['keuangan_createdby']  = $this->me->user_id;
          $ins['keuangan_createdtime']= date("Y-m-d H:i:s");
          $this->model_keuangan->tambah('keuangan', $ins);
        }
        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menyimpan anggaran'));

      }

      $data['desa_id']= $desa_id;
      $data['title']  = "Keuangan";
      $data['tahun']  = !empty($tahun) ? $tahun : date("Y");
      $data['apbd']   = $this->getAPBDbyKategori($data['tahun']);
      $data['desa']   = $this->model_keuangan->getListDesa();
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain($data['tahun'], $desa_id);
      $this->template->load('keuangan/index', $data);
    }

    public function json($tahun = 0){

      $desa_id  = $this->me->desa_id;

      if( $this->me->user_role_id == 1 && !empty($_GET['desa_id']) ){
        $desa_id  = (int)$_GET['desa_id'];
      }

      $data['apbd']     = $this->getAPBDbyKategori($tahun, $desa_id);
      $data['keuangan'] = $this->model_keuangan->getListKeuanganLain($tahun, $desa_id);

      echo json_encode($data);
    }

    protected function getAPBDbyKategori($tahun = 0, $desa_id = NULL){
      $apbd = $this->getAPBD($tahun, $desa_id);
      $res  = array();
      foreach ($apbd as $k => $ap) {
        if(empty($res[$ap['kategori']])){
          $res[$ap['kategori']] = array();
        }
        array_push($res[$ap['kategori']], $ap);
      }
      return $res;
    }

    protected function getAPBD($tahun = 0, $desa_id = NULL){
      $desa_id  = !empty($desa_id) ? $desa_id : $this->me->desa_id;
      $tahun= (int)$tahun;
      $apbd = $this->model_keuangan->getListAPBD();
      $data = array();
      foreach($apbd as $k => $ap){
        $res  = $this->model_keuangan->getKeuangan($ap->apbd_id, $tahun, $desa_id);
        $data[$k]['id']     = $ap->apbd_id;
        $data[$k]['apbd']   = $ap->apbd_nama;
        $data[$k]['kategori'] = $ap->apbd_kategori;
        $data[$k]['res']    = !empty($res) ? $res->keuangan_anggaran : 0;
      }
      return $data;
    }

    public function pemakaian($goto='', $num = 0){
      switch($goto){
        case 'add' : {
          $this->pemakaian_add();
          break;
        }
        case 'delete' : {
          $this->pemakaian_delete($num);
          break;
        }
        case 'json' : {
          $this->pemakaian_json($num);
          break;
        }
        default: {
          $this->pemakaian_index($goto);
          break;
        }
      }
    }

    protected function pemakaian_json($num){
      $tahun    = $num != 0 ? $num : date("Y");
      $desa_id  = $this->me->desa_id;
      $id_desa  = (int)$_GET['desa_id'];

      if( $this->me->user_role_id == 1 && !empty($id_desa) ){
        $desa_id  = $id_desa;
      }

      $data['desa_id']= $desa_id;
      $data['apbd']   = $this->model_keuangan->getListAPBD();
      $data['title']  = "Pemakaian Anggaran";
      $data['pemakaian']  = $this->beautyPemakaianDetail($desa_id, $tahun);
      $this->load->view('keuangan/pemakaian_json', $data);
    }

    protected function pemakaian_delete($num){
      $desa_id  = $this->me->desa_id;
      if( $this->me->user_role_id == 1 ){
        $desa_id  = NULL;
      }
      $this->model_keuangan->deletePemakaian($desa_id, $num);
      $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menghapus pemakaian keuangan'));
      kembali('keuangan/pemakaian');
    }

    protected function pemakaian_index($num = 0){
      $tahun    = $num != 0 ? $num : date("Y");
      $desa_id  = $this->me->desa_id;
      $data['tahun']  = $tahun;
      $data['desa_id']= $desa_id;
      $data['desa']   = $this->model_keuangan->getListDesa();
      $data['apbd']   = $this->model_keuangan->getListAPBD();
      $data['title']  = "Pemakaian Anggaran";
      $data['pemakaian']  = $this->beautyPemakaianDetail($desa_id, $tahun);
      $this->template->load('keuangan/pemakaian', $data);
    }

    public function beautyPemakaianDetail($desa_id, $tahun){
      $pemakaian  = $this->model_keuangan->getListPemakaianDetail($desa_id, $tahun);
      $apbd       = $this->model_keuangan->getListAPBD();
      $baru = array();
      foreach($pemakaian as $pem){
        if( empty( $baru[$pem->kk_kode] ) ){
          $baru[$pem->kk_kode]  = array('uraian'=>$pem->kk_uraian, 'id'=>$pem->kk_id, 'kode'=>$pem->kk_kode, 'pemakaian'=>array());
        }
        $pakai  = explode(',', $pem->pemakaian);
        $arp    = array();
        unset($a);
        foreach($pakai as $pak){
          $p  = explode(':', $pak);
          $a[$p[0]] = $p[2];
        }
        $b  = array();
        foreach ($apbd as $ad) {
          $b[$ad->apbd_id]  = !empty($a[$ad->apbd_id]) ? $a[$ad->apbd_id] : 0;
        }
        array_push($baru[$pem->kk_kode]['pemakaian'], array( 'uraian'=>$pem->kp_uraian, 'pemakaian'=>$b ));
      }

      return $baru;
    }

    protected function pemakaian_add(){
      $desa_id  = $this->me->desa_id;
      $this->form_validation->set_rules('kp_tahun', 'tahun', 'required|numeric');
      if( $this->me->user_role_id == 1 ){
        $this->form_validation->set_rules('kp_desa', 'desa', 'required|numeric');
      }
      $this->form_validation->set_rules('kk_id', 'kegiatan', 'required|numeric');

      if( $this->form_validation->run() != false ){
        $ins['kp_tahun']  = $this->input->post('kp_tahun');
        $ins['kk_id']     = $this->input->post('kk_id');

        if( $this->me->user_role_id == 1 ){
          $desa_id  = $this->input->post('kp_desa');
        }

        $kegiatan = $this->input->post('kegiatan');
        foreach($kegiatan as $keg){
          $sub  = true;
          $sub  = !empty(trim($keg['uraian'])) ? $sub : false;
          $ber  = array();
          if( $sub ){
            foreach($keg['sumber'] as $sum){
              if( !empty($sum['jumlah']) ){
                array_push($ber, $sum);
              }
            }
            if( !empty($ber) ){
              $ins['desa_id']   = $desa_id;
              $ins['kp_uraian'] = $keg['uraian'];
              $kp_id  = $this->model_keuangan->tambah('keuangan_pemakaian', $ins);
              foreach($ber as $be){
                $ind['kp_id']   = $kp_id;
                $ind['apbd_id'] = $be['apbd'];
                $ind['kpd_jumlah']  = $be['jumlah'];
                $this->model_keuangan->tambah('keuangan_pemakaian_detail', $ind);
              }
              $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan kegiatan anggaran'));
            }
          }
        }
      }

      $data['desa']   = $this->model_keuangan->getListDesa();
      $data['apbd']   = $this->model_keuangan->getListAPBD();
      $data['bidang'] = $this->model_keuangan->getListBidang();
      $data['title']  = "Tambah Pemakaian";
      $this->template->load('keuangan/pemakaian_add', $data);
    }

    public function jsonKegiatan($bidang = 0){
      echo json_encode($this->model_keuangan->getKegiatanByBidang($bidang));
    }

  }
