<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Auth extends MY_Controller{

    public function __construct(){
      parent::__construct();
      $this->load->model('model_auth');
    }

    public function index(){
      $this->template->setTemplate('template/not_loggedin');

      $this->form_validation->set_rules('username', 'username', 'required');
      $this->form_validation->set_rules('password', 'password', 'required');

      if( $this->form_validation->run() != false ){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user     = $this->model_auth->getUser($username);
        if( $user && password_verify($password.$user->user_password_salt, $user->user_password) ){
          $user = json_encode($user);
          $user = $this->encryption->encrypt($user);
          set_cookie('user', $user, 3600*24);
          redirect('dashboard');
        }else{
          $this->config->set_item('informasi', informasi('danger', 'Silahkan cek kembali username dan password.'));
        }
      }

      $data['title']  = "Login";
      $this->template->load('auth/index', $data);
    }

    public function logout(){
      delete_cookie('user');
      kembali('auth');
    }

  }
