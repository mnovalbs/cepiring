<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Warga extends MY_Controller{
    private $me;

    public function __construct(){
      parent::__construct();
      $this->load->model('model_desa');
      $this->me = $this->login_required_perangkat_desa();
      $this->template->setTemplate('template/cepiring');
      $this->load->model('model_warga');
    }

    public function index(){
      $desa_id  = $this->me->user_desa_id;
      if( $this->me->user_role_id == 1 ){
        $desa_id  = NULL;
      }
      $data['warga']  = $this->model_warga->getListWarga($this->me->user_id, $desa_id);
      $data['title']  = "Warga";
      $this->template->load('warga/index', $data);
    }

    public function edit($warga_id = 0){
      $warga_id = empty($warga_id) ? $this->me->user_id : $warga_id;
      $user_id  = $this->me->user_id;
      if( !empty($this->me->desa_perangkat_level) || $this->me->user_role_id == 1 ){
        $user_id  = NULL;
      }
      $desa_id  = $this->me->user_desa_id;
      if( $this->me->user_role_id == 1 ){
        $desa_id  = NULL;
      }
      $desa_id  = empty($warga_id) ? NULL : $desa_id;

      $warga  = $this->model_warga->getWarga($warga_id, $desa_id);
      if(empty($warga)){
        $this->session->set_flashdata('pesan', informasi('danger', 'Warga tidak ditemukan'));
        redirect('warga');
      }

      $this->form_validation->set_rules('user_name', 'nama warga', 'required|trim|max_length[40]|regex_match[/[a-zA-Z][a-zA-Z ]{1,38}/]');
      $this->form_validation->set_rules('user_password', 'password warga', 'min_length[8]');
      if( $this->me->user_role_id == 1 ){
        $this->form_validation->set_rules('user_desa_id', 'desa warga', 'required|numeric|callback_isDesaExists');
        $this->form_validation->set_rules('user_role_id', 'role warga', 'required|numeric|in_list[1,3]');
      }
      $this->form_validation->set_rules('user_kontak', 'nomor telepon', 'max_length[16]');
      $this->form_validation->set_rules('user_lahir_tempat', 'tempat lahir', 'max_length[40]');
      $this->form_validation->set_rules('user_jenis_kelamin', 'jenis kelamin', 'in_list[1,2]');
      $this->form_validation->set_rules('user_agama', 'agama', 'in_list[1,2,3,4,5]');
      $this->form_validation->set_rules('user_status_perkawinan', 'status perkawinan', 'in_list[1,2]');
      $this->form_validation->set_rules('user_pendidikan', 'pendidikan', 'max_length[20]');

      if( $this->form_validation->run() != false ){
        if( !empty($this->input->post('user_password')) ){
          $upd['user_password_salt']  = sha1( rand(1,100).rand(1,100).rand(1,100).rand(1,100).date("YmdHis") );
          $upd['user_password']       = password_hash($this->input->post('user_password').$upd['user_password_salt'], PASSWORD_DEFAULT);
        }
        $upd['user_name']     = $this->input->post('user_name');
        if( $this->me->user_role_id == 1 ){
          $upd['user_desa_id']  = $this->input->post('user_desa_id');
          $upd['user_role_id']  = $this->input->post('user_role_id');
        }

        $this->model_warga->edit($warga->user_id, $upd);
        $this->model_warga->resetUserInfo($warga->user_id);

        $inf['user_lahir_tempat']   = $this->input->post('user_lahir_tempat');
        $inf['user_lahir_tanggal']  = $this->input->post('user_lahir_tanggal');
        $inf['user_jenis_kelamin']  = $this->input->post('user_jenis_kelamin');
        $inf['user_status_perkawinan'] = $this->input->post('user_status_perkawinan');
        $inf['user_agama']          = $this->input->post('user_agama');
        $inf['user_pendidikan']     = $this->input->post('user_pendidikan');
        $inf['user_kontak']         = $this->input->post('user_kontak');
        $inf['user_id']             = $warga->user_id;

        $this->model_warga->add('user_info', $inf);

        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil mengubah informasi warga.'));
        if( $warga_id == $this->me->user_id ){
          redirect('profile');
        }else{
          redirect('warga');
        }
      }

      $data['warga']['warga'] = $warga;
      $data['desa']           = $this->model_desa->getListDesa();
      $data['title']          = "Ubah Warga";
      $this->template->load('warga/edit', $data);
    }

    public function add(){
      $this->form_validation->set_rules('user_name', 'nama warga', 'required|trim|max_length[40]|regex_match[/[a-zA-Z][a-zA-Z ]{1,38}/]');
      $this->form_validation->set_rules('user_username', 'username warga', 'required|trim|regex_match[/^[a-zA-Z][a-zA-Z0-9_]{3,11}$/]|callback_isWargaAvailable');
      $this->form_validation->set_rules('user_password', 'password warga', 'required|min_length[8]');
      if( $this->me->user_role_id == 1 ){
        $this->form_validation->set_rules('user_desa_id', 'desa warga', 'required|numeric|callback_isDesaExists');
        $this->form_validation->set_rules('user_role_id', 'role warga', 'required|numeric|in_list[1,3]');
      }
      $this->form_validation->set_rules('user_kontak', 'nomor telepon', 'max_length[16]');
      $this->form_validation->set_rules('user_lahir_tempat', 'tempat lahir', 'max_length[40]');
      $this->form_validation->set_rules('user_jenis_kelamin', 'jenis kelamin', 'in_list[1,2]');
      $this->form_validation->set_rules('user_agama', 'agama', 'in_list[1,2,3,4,5]');
      $this->form_validation->set_rules('user_status_perkawinan', 'status perkawinan', 'in_list[1,2]');
      $this->form_validation->set_rules('user_pendidikan', 'pendidikan', 'max_length[20]');

      if( $this->form_validation->run() != false ){
        $ins['user_password']       = $this->input->post('user_password');
        $ins['user_password_salt']  = sha1( rand(1,100).rand(1,100).rand(1,100).rand(1,100).date("YmdHis") );
        $ins['user_password']       = password_hash($ins['user_password'].$ins['user_password_salt'], PASSWORD_DEFAULT);
        $ins['user_name']           = $this->input->post('user_name');
        $ins['user_username']       = $this->input->post('user_username');
        if( $this->me->user_role_id == 1 ){
          $ins['user_desa_id']        = $this->input->post('user_desa_id');
          $ins['user_role_id']        = $this->input->post('user_role_id');
        }else{
          $ins['user_desa_id']        = $this->me->user_desa_id;
          $ins['user_role_id']        = 3;
        }
        $ins['user_createdtime']    = date("Y-m-d H:i:s");
        $ins['user_createdby']      = $this->me->user_id;
        $ins['user_key']            = sha1( date("YmdHis").rand(1,100).rand(1,100).rand(1,100).rand(1,100).rand(1,100) );

        $user_id  = $this->model_warga->add('user', $ins);

        $inf['user_lahir_tempat']   = $this->input->post('user_lahir_tempat');
        $inf['user_lahir_tanggal']  = $this->input->post('user_lahir_tanggal');
        $inf['user_jenis_kelamin']  = $this->input->post('user_jenis_kelamin');
        $inf['user_status_perkawinan'] = $this->input->post('user_status_perkawinan');
        $inf['user_agama']          = $this->input->post('user_agama');
        $inf['user_pendidikan']     = $this->input->post('user_pendidikan');
        $inf['user_kontak']         = $this->input->post('user_kontak');
        $inf['user_id']             = $user_id;

        $this->model_warga->add('user_info', $inf);

        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan warga baru.'));
        redirect('warga');
      }

      $data['title']  = "Tambah Warga";
      $data['desa']   = $this->model_desa->getListDesa();
      $this->template->load('warga/add', $data);
    }

    public function isWargaAvailable($username){
      if( $this->model_warga->isWargaAvailable($username) ){
        return true;
      }else{
        $this->form_validation->set_message('isWargaAvailable', 'username tidak dapat dipakai');
        return false;
      }
    }

    public function isDesaExists($desa_id){
      if( $this->model_desa->isDesaExists($desa_id) != false ){
        return true;
      }else {
        $this->form_validation->set_message('isDesaExists', 'Desa tidak ditemukan.');
        return false;
      }
    }

  }
