<?php

  class Apbd extends MY_Controller{

    private $me;

    public function __construct(){
      parent::__construct();

      $this->me = $this->login_required_perangkat_kecamatan();
      $this->template->setTemplate('template/cepiring');
      $this->load->model('model_apbd');
    }

    public function index(){

      if( isset($_POST['apbd_id']) ){
        $this->editApbd();
      }else{
        $this->addApbd();
      }

      $data['title']  = "Daftar APBD";
      $data['apbd']   = $this->model_apbd->getListApbd();
      $this->template->load('apbd/index', $data);
    }

    public function delete($num=0){
      $this->model_apbd->hapus($num);
      redirect('apbd');
    }

    public function kegiatan($goto = '', $num = 0){
      switch($goto){
        case 'add' : {
          $this->kegiatan_add();
          break;
        }
        case 'edit' : {
          $this->kegiatan_edit($num);
          break;
        }
        case 'delete' : {
          $this->kegiatan_delete($num);
          break;
        }
        default : {
          $data['kegiatan'] = $this->model_apbd->getListKegiatan();
          $data['title']    = "Kegiatan Keuangan";
          $this->template->load('apbd/kegiatan', $data);
          break;
        }
      }
    }

    public function bidang($goto = '', $num = 0){
      switch($goto){
        case 'delete' : {
          $this->bidang_hapus($num);
          break;
        }
        case 'edit' : {
          $this->bidang_edit($num);
          break;
        }
        default : {
          $this->bidang_index();
          break;
        }
      }
    }

    protected function bidang_edit($num){
      $this->form_validation->set_rules('nama_bidang', 'nama bidang', 'required|trim|max_length[100]');
      $out['res'] = false;

      if( $this->form_validation->run() != false ){
        $upd['kb_nama'] = $this->input->post('nama_bidang');
        $this->model_apbd->update_bidang($num, $upd);
        $out['res'] = true;
      }
      echo json_encode($out);
    }

    protected function bidang_hapus($num){
      $this->model_apbd->delete_bidang($num);
      $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menghapus bidang keuangan'));
      redirect('apbd/bidang');
    }

    protected function bidang_index(){
      if( isset($_POST['tambah_bidang']) ){
        $this->form_validation->set_rules('kk_nama', 'nama bidang', 'required|max_length[100]');

        if( $this->form_validation->run() != false ){
          $ins['kb_nama'] = $this->input->post('kk_nama');
          $this->model_apbd->tambah('keuangan_bidang', $ins);
          $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan bidang keuangan baru'));
        }
      }

      $data['bidang'] = $this->model_apbd->getListBidang();
      $data['title']  = "Daftar Bidang APBD";
      $this->template->load('apbd/bidang', $data);
    }

    protected function kegiatan_edit($num){
      $kegiatan = $this->model_apbd->edit_kegiatan($num);
      if( empty($kegiatan) ){
        $this->session->set_flashdata('pesan', informasi('danger', 'Tidak dapat menemukan kegiatan tersebut'));
        redirect('apbd/kegiatan');
      }

      $this->form_validation->set_rules('kk_bidang', 'bidang kegiatan', 'required|numeric');
      $this->form_validation->set_rules('kk_uraian', 'nama kegiatan', 'required|max_length[100]');

      if( $this->form_validation->run() != false ){
        $ins['kk_bidang'] = $this->input->post('kk_bidang');
        $ins['kk_uraian'] = $this->input->post('kk_uraian');

        $this->model_apbd->update_kegiatan($kegiatan->kk_id, $ins);
        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan kegiatan keuangan.'));
        redirect('apbd/kegiatan');
      }

      $data['kegiatan']['kegiatan'] = $kegiatan;
      $data['title']  = "Edit Kegiatan : ".$kegiatan->kk_uraian;
      $data['bidang'] = $this->model_apbd->getListBidang();
      $this->template->load('apbd/kegiatan_edit', $data);
    }

    protected function kegiatan_delete($num){
      $this->model_apbd->delete_kegiatan($num);
      $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menghapus kegiatan'));
      redirect('apbd/kegiatan');
    }

    protected function kegiatan_add(){
      $this->form_validation->set_rules('kk_kode', 'kode kegiatan', 'required|numeric|max_length[4]|callback_isKegiatanAvailable');
      $this->form_validation->set_rules('kk_bidang', 'bidang kegiatan', 'required|numeric');
      $this->form_validation->set_rules('kk_uraian', 'nama kegiatan', 'required|max_length[100]');

      if( $this->form_validation->run() != false ){
        $ins['kk_kode']   = $this->input->post('kk_kode');
        $ins['kk_bidang'] = $this->input->post('kk_bidang');
        $ins['kk_uraian'] = $this->input->post('kk_uraian');

        $this->model_apbd->tambah('keuangan_kegiatan', $ins);
        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan kegiatan keuangan.'));
        redirect('apbd');
      }

      $data['title']  = "Tambah Kegiatan";
      $data['bidang'] = $this->model_apbd->getListBidang();
      $this->template->load('apbd/kegiatan_add', $data);
    }

    protected function editApbd(){
      $this->form_validation->set_rules('apbd_kategori', 'kategori anggaran', 'required|in_list[1,2]');
      $this->form_validation->set_rules('apbd_nama', 'nama anggaran', 'required|trim|max_length[40]');
      $this->form_validation->set_rules('apbd_id', 'anggaran', 'required|trim|numeric');

      if( $this->form_validation->run() != false ){
        $apbd_id          = $this->input->post('apbd_id');
        $upd['apbd_nama'] = $this->input->post('apbd_nama');
        $upd['apbd_kategori'] = $this->input->post('apbd_kategori');
        $this->model_apbd->edit($apbd_id, $upd);
      }
    }

    protected function addApbd(){
      $this->form_validation->set_rules('apbd_nama', 'nama anggaran', 'required|trim|max_length[40]|callback_isApbdAvailable');
      $this->form_validation->set_rules('apbd_kategori', 'kategori anggaran', 'required|in_list[1,2]');

      if( $this->form_validation->run() != false ){
        $ins['apbd_kategori']   = $this->input->post('apbd_kategori');
        $ins['apbd_nama']       = $this->input->post('apbd_nama');
        $ins['apbd_createdby']  = $this->me->user_id;
        $ins['apbd_createdtime']= date("Y-m-d H:i:s");

        $this->model_apbd->tambah('apbd', $ins);
        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil menambahkan apbd baru'));
      }
    }

    public function isKegiatanAvailable($kode){
      if( $this->model_apbd->isKegiatanAvailable($kode) ){
        return true;
      }else{
        $this->form_validation->set_message('isKegiatanAvailable', 'Kode sudah digunakan.');
        return false;
      }
    }

    public function isApbdAvailable($apbd_nama){
      if( $this->model_apbd->isApbdAvailable($apbd_nama) != false ){
        return true;
      }else{
        $this->form_validation->set_message('isApbdAvailable', 'APBD Sudah Terdaftar.');
        return false;
      }
    }

  }
