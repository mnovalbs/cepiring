<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Umkm extends MY_Controller{

    private $me;

    public function __construct(){
      parent::__construct();
      $this->me = $this->login_required_user();
      $this->template->setTemplate('template/cepiring');
      $this->load->model('model_umkm');
    }

    public function index(){
      $user_id  = $this->me->user_id;
      if( !empty($this->me->desa_perangkat_level) || $this->me->user_role_id == 1 ){
        $user_id  = NULL;
      }
      $desa_id  = $this->me->user_desa_id;
      if( $this->me->user_role_id == 1 ){
        $desa_id  = NULL;
      }
      $data['produk'] = $this->model_umkm->getListProduk($user_id, $desa_id);
      $data['title']  = "Daftar Produk";
      $this->template->load('umkm/index', $data);
    }

    public function edit($num = 0){
      $user_id  = $this->me->user_id;
      if( !empty($this->me->desa_perangkat_level) || $this->me->user_role_id == 1 ){
        $user_id  = NULL;
      }
      $desa_id  = $this->me->user_desa_id;
      if( $this->me->user_role_id == 1 ){
        $desa_id  = NULL;
      }
      $data['produk']['produk'] = $this->model_umkm->getDetailProduk($num, $user_id, $desa_id);
      if( empty($data['produk']['produk']) ){
        $this->session->set_flashdata('pesan', informasi('danger', 'Produk tidak ditemukan'));
        redirect('umkm');
      }

      $this->form_validation->set_rules('produk_nama', 'nama produk', 'required|trim|max_length[80]');
      $this->form_validation->set_rules('produk_deskripsi', 'deskripsi produk', 'trim');
      $this->form_validation->set_rules('produk_harga', 'harga produk', 'required|numeric|less_than_equal_to[500000000]|greater_than[0]');
      $this->form_validation->set_rules('produk_status', 'status produk', 'required|in_list[1,2]');
      $this->form_validation->set_rules('produk_satuan', 'satuan produk', 'required|trim|max_length[20]');

      if($this->form_validation->run()){
        $upd['produk_nama']       = $this->input->post('produk_nama');
        $upd['produk_deskripsi']  = $this->input->post('produk_deskripsi');
        $upd['produk_harga']      = $this->input->post('produk_harga');
        $upd['produk_status']     = $this->input->post('produk_status');
        $upd['produk_satuan']     = $this->input->post('produk_satuan');

        $this->model_umkm->ubahProduk($num, $upd);

        $desa_path    = FCPATH.'/assets/file_desa/'.$this->me->desa_key;
        $user_path    = $desa_path.'/'.$this->me->user_key;

        foreach( $this->input->post('gambar_hapus') as $gambar_hapus ){
          $this->model_umkm->hapusGambar($gambar_hapus);
        }

        foreach( $_FILES['produk_gambar']['name'] as $k => $produk_gambar ){
          $_FILES['userfile']['name']     = $_FILES['produk_gambar']['name'][$k];
          $_FILES['userfile']['type']     = $_FILES['produk_gambar']['type'][$k];
          $_FILES['userfile']['tmp_name'] = $_FILES['produk_gambar']['tmp_name'][$k];
          $_FILES['userfile']['error']    = $_FILES['produk_gambar']['error'][$k];
          $_FILES['userfile']['size']     = $_FILES['produk_gambar']['size'][$k];
          if( $_FILES['userfile']['error'] === 0 ){
            $up = $this->upload('userfile', $user_path);
            if( $up != false ){
              $ph['produk_gambar_url']    = $up['file_name'];
              $ph['produk_id']            = $num;
              $this->model_umkm->add('produk_gambar', $ph);
              $this->beauty_crop_image(250, 172, $user_path, $up['file_name']);
              $this->beauty_crop_image(100, 100, $user_path, $up['file_name']);
            }
          }
        }

        $this->session->set_flashdata('pesan', informasi('success', 'Berhasil mengubah produk.'));
        redirect('umkm');
      }

      $data['gambar'] = $this->model_umkm->getProdukGambar($num);
      $data['title']  = "Ubah Produk";
      $this->template->load('umkm/edit', $data);
    }

    public function add(){

      $this->form_validation->set_rules('produk_nama', 'nama produk', 'required|trim|max_length[80]');
      $this->form_validation->set_rules('produk_deskripsi', 'deskripsi produk', 'trim');
      $this->form_validation->set_rules('produk_harga', 'harga produk', 'required|numeric|less_than_equal_to[500000000]|greater_than[0]');
      $this->form_validation->set_rules('produk_status', 'status produk', 'required|in_list[1,2]');
      $this->form_validation->set_rules('produk_satuan', 'satuan produk', 'required|trim|max_length[20]');

      if($this->form_validation->run()){
        $ins['produk_nama']       = $this->input->post('produk_nama');
        $ins['produk_deskripsi']  = $this->input->post('produk_deskripsi');
        $ins['produk_harga']      = $this->input->post('produk_harga');
        $ins['produk_status']     = $this->input->post('produk_status');
        $ins['produk_satuan']     = $this->input->post('produk_satuan');
        $ins['produk_url']        = slug($ins['produk_nama']);
        $ins['user_id']           = $this->me->user_id;

        $num = 1;
        while( !$this->model_umkm->isProductAvailable($this->me->user_id, $ins['produk_url']) ){
          $ins['produk_url']  = slug($ins['produk_nama'])."-".$num;
          $num++;
        }

        $produk_id    = $this->model_umkm->add('produk', $ins);
        $desa_path    = FCPATH.'/assets/file_desa/'.$this->me->desa_key;
        $user_path    = $desa_path.'/'.$this->me->user_key;
        if( !file_exists($desa_path) ){
          mkdir($desa_path, 0777);
        }
        if( !file_exists($user_path) ){
          mkdir($user_path, 0777);
        }

        foreach( $_FILES['produk_gambar']['name'] as $k => $produk_gambar ){
          $_FILES['userfile']['name']     = $_FILES['produk_gambar']['name'][$k];
          $_FILES['userfile']['type']     = $_FILES['produk_gambar']['type'][$k];
          $_FILES['userfile']['tmp_name'] = $_FILES['produk_gambar']['tmp_name'][$k];
          $_FILES['userfile']['error']    = $_FILES['produk_gambar']['error'][$k];
          $_FILES['userfile']['size']     = $_FILES['produk_gambar']['size'][$k];
          if( $_FILES['userfile']['error'] === 0 ){
            $up = $this->upload('userfile', $user_path);
            if( $up != false ){
              $ph['produk_gambar_url']    = $up['file_name'];
              $ph['produk_id']            = $produk_id;
              $this->beauty_crop_image(250, 172, $user_path, $up['file_name']);
              $this->beauty_crop_image(100, 100, $user_path, $up['file_name']);
              $this->model_umkm->add('produk_gambar', $ph);
            }
          }
        }

        $this->session->set_flashdata('pesan', informasi('success', 'Produk berhasil ditambahkan'));
        redirect('umkm');
      }

      $data['title']  = "Tambah Produk";
      $this->template->load('umkm/add', $data);
    }

  }
