<?php
  defined('BASEPATH') or die('Tidak dapat diakses langsung!');

  class Template{
    private $folder = '';
    private $template = '';

    public function setFolder($str = ''){
      $this->folder = $str;
    }

    public function setTemplate($str = ''){
      $this->template = $str;
    }

    public function load($view = '', $data = array()){
      $ci =& get_instance();
      $ci->load->library('parser');
      $views['content'] = $ci->parser->parse($view, $data, true);
      $ci->parser->parse($this->template, $views);
    }

  }
