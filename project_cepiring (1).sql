-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 08, 2017 at 05:34 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_cepiring`
--

-- --------------------------------------------------------

--
-- Table structure for table `apbd`
--

CREATE TABLE `apbd` (
  `apbd_id` int(11) NOT NULL,
  `apbd_nama` varchar(40) NOT NULL,
  `apbd_createdby` int(11) DEFAULT NULL,
  `apbd_createdtime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apbd`
--

INSERT INTO `apbd` (`apbd_id`, `apbd_nama`, `apbd_createdby`, `apbd_createdtime`) VALUES
(1, 'Bantuan Desa', 1, '2017-08-07 06:00:00'),
(2, 'Anggaran Lain Edited', 1, '2017-08-07 06:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `desa_id` int(11) NOT NULL,
  `desa_name` varchar(40) NOT NULL,
  `desa_address` text,
  `desa_createdby` int(11) DEFAULT NULL,
  `desa_createdtime` timestamp NULL DEFAULT NULL,
  `desa_url` varchar(40) NOT NULL,
  `desa_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`desa_id`, `desa_name`, `desa_address`, `desa_createdby`, `desa_createdtime`, `desa_url`, `desa_key`) VALUES
(1, 'Korowelang Anyar', 'Korowelang Anyar disini alamat lengkapnya yeay', NULL, NULL, 'korowelang-anyar', 'b140c90fc16f6fd022f0c41310d1312ecfe2f90c'),
(4, 'Korowelang Kulon', 'Korowelang jarene disini', 1, '2017-07-30 11:45:48', 'korowelang-kulon', '95a359f6c28efd33fc2f42afadeecd33bca69ecb');

-- --------------------------------------------------------

--
-- Table structure for table `desa_jabatan`
--

CREATE TABLE `desa_jabatan` (
  `desa_jabatan_id` int(11) NOT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `desa_jabatan_name` varchar(40) NOT NULL,
  `desa_jabatan_level` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_perangkat`
--

CREATE TABLE `desa_perangkat` (
  `desa_perangkat_id` int(11) NOT NULL,
  `desa_perangkat_jabatan` varchar(40) NOT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `desa_perangkat_level` smallint(6) NOT NULL,
  `desa_perangkat_sk_no` varchar(40) DEFAULT NULL,
  `desa_perangkat_sk_tgl` date DEFAULT NULL,
  `desa_perangkat_nip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa_perangkat`
--

INSERT INTO `desa_perangkat` (`desa_perangkat_id`, `desa_perangkat_jabatan`, `desa_id`, `user_id`, `desa_perangkat_level`, `desa_perangkat_sk_no`, `desa_perangkat_sk_tgl`, `desa_perangkat_nip`) VALUES
(17, 'Kepala Desa', 1, 1, 1, '123456', '2016-01-01', ''),
(18, 'Kepala Desa', 4, 2, 1, '', '0000-00-00', ''),
(19, 'Carik', 4, 4, 2, '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `history_id` int(11) NOT NULL,
  `history_table_name` varchar(20) NOT NULL,
  `history_changeby_id` int(11) NOT NULL,
  `history_changeby_name` varchar(40) NOT NULL,
  `history_meta` text NOT NULL,
  `history_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `history_type` enum('1','2') NOT NULL COMMENT '1=>Update, 2=>Delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `keuangan`
--

CREATE TABLE `keuangan` (
  `keuangan_id` int(11) NOT NULL,
  `apbd_id` int(11) DEFAULT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `keuangan_tahun` smallint(4) NOT NULL,
  `keuangan_anggaran` bigint(20) NOT NULL,
  `keuangan_createdby` int(11) DEFAULT NULL,
  `keuangan_createdtime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuangan`
--

INSERT INTO `keuangan` (`keuangan_id`, `apbd_id`, `desa_id`, `keuangan_tahun`, `keuangan_anggaran`, `keuangan_createdby`, `keuangan_createdtime`) VALUES
(29, 1, 4, 2018, 1000000, 2, '2017-08-07 14:55:28'),
(30, 2, 4, 2018, 2000000, 2, '2017-08-07 14:55:28'),
(39, 1, 4, 2017, 1000000000, 2, '2017-08-08 10:32:41'),
(40, 2, 4, 2017, 250000, 2, '2017-08-08 10:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `keuangan_lain`
--

CREATE TABLE `keuangan_lain` (
  `keuangan_lain_id` int(11) NOT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `keuangan_lain_uraian` varchar(50) NOT NULL,
  `keuangan_lain_keterangan` varchar(300) NOT NULL,
  `keuangan_lain_tahun` smallint(4) NOT NULL,
  `keuangan_lain_bulan` int(2) DEFAULT NULL,
  `keuangan_lain_anggaran` bigint(20) NOT NULL,
  `keuangan_lain_createdby` int(11) DEFAULT NULL,
  `keuangan_lain_createdtime` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuangan_lain`
--

INSERT INTO `keuangan_lain` (`keuangan_lain_id`, `desa_id`, `keuangan_lain_uraian`, `keuangan_lain_keterangan`, `keuangan_lain_tahun`, `keuangan_lain_bulan`, `keuangan_lain_anggaran`, `keuangan_lain_createdby`, `keuangan_lain_createdtime`) VALUES
(16, 4, 'Beli Semen', 'Ini keterangan', 2017, 1, -500000, 2, '2017-08-08 10:32:41'),
(17, 4, 'Beli Bangunan', '', 2017, 3, -100000, 2, '2017-08-08 10:32:41'),
(18, 4, 'Beli Sesuatu', '', 2017, 9, -100000, 2, '2017-08-08 10:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `produk_nama` varchar(40) NOT NULL,
  `produk_harga` bigint(20) NOT NULL,
  `produk_satuan` varchar(20) NOT NULL,
  `produk_status` enum('1','2') NOT NULL DEFAULT '1',
  `produk_deskripsi` text NOT NULL,
  `produk_url` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `user_id`, `produk_nama`, `produk_harga`, `produk_satuan`, `produk_status`, `produk_deskripsi`, `produk_url`) VALUES
(1, NULL, 'Coba Nama Produk', 10000, 'kg', '1', '<p>cobatest</p>', ''),
(2, 1, 'Coba Nama Produk Cepirings', 10000, 'kg', '2', '<p>cobatest</p>', ''),
(3, 1, 'Terasi', 10000, 'sack', '1', '<p>Terasinya joss</p>', 'terasi'),
(4, 1, 'Terasi', 10000, 'sack', '1', '<p>Terasinya joss</p>', 'Terasi-1'),
(5, 1, 'Terasi', 10000, 'sack', '1', '<p>Terasinya joss</p>', 'Terasi-2'),
(6, 1, 'Ikan Asap', 1000, 'ikan', '2', '<p>test</p>', 'ikan-asap'),
(7, NULL, 'ProdukQu', 60000, 'kg', '1', '<p>ehehehe</p>', 'produkqu'),
(8, 3, 'ProdukQu Harits', 99000, 'kg', '1', '', 'produkqu-harits');

-- --------------------------------------------------------

--
-- Table structure for table `produk_gambar`
--

CREATE TABLE `produk_gambar` (
  `produk_gambar_id` int(11) NOT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `produk_gambar_url` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_gambar`
--

INSERT INTO `produk_gambar` (`produk_gambar_id`, `produk_id`, `produk_gambar_url`) VALUES
(1, 1, 'b7b11abc31a34fc0ddb79445f3400640.png'),
(2, 1, '9e0e3eb9467d5c0811b88f272a054450.png'),
(3, 2, 'ed2c846648cc51b06e58f019bdc1f521.png'),
(5, 4, 'c32bde0226b7e2056f8742701877cf34.jpg'),
(6, 5, '322e7bc7bab7d5ed655816b7427cfd85.jpg'),
(7, 2, '6c8005cc5b9943702b0fdec8938d1c46.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `user_username` varchar(12) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_password_salt` varchar(40) NOT NULL,
  `user_createdby` int(11) DEFAULT NULL,
  `user_createdtime` timestamp NULL DEFAULT NULL,
  `user_role_id` int(11) NOT NULL,
  `user_desa_id` int(11) NOT NULL,
  `user_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_username`, `user_password`, `user_password_salt`, `user_createdby`, `user_createdtime`, `user_role_id`, `user_desa_id`, `user_key`) VALUES
(1, 'Muhammad Noval Bintang Salim', 'mnovalbs', '$2y$10$kP3SjMg8PmO/LfwjOwZEee63jcFevlfKgJmR2QwmoK/OsonMT48hS', 'd53d66f606ae8f97c6102ec4521adabe332fb62e', NULL, NULL, 1, 1, 'a9ca183215e71d008b62ed821d50ce4512e6d21f'),
(2, 'Muhammad Noval Bintang Salim', 'novalbs', '$2y$10$SIWkwo.1lKHSxZw3WYO.dOQCsc3s4ZePThly5GeS/OxpX6I2.mYmG', '21b9921b21957aadf2872ff71df7c81926898c85', 1, '2017-08-02 02:01:24', 3, 4, 'af3e133428b9e25c55bc59fe534248e6a0c0f17b'),
(3, 'Harits Mugni', 'haritsmn', '$2y$10$D9nFsbGCNinx1.IjBpRbaernae6kMTZRBC/IQGD21rgPYXApM1pPm', '7f3d1853907f8653bd3492c9ff618b9e8c9967d2', 1, '2017-08-02 02:11:21', 3, 4, 'c2da4d2da326df91548d9aab1b7b4dbe4fe91102'),
(4, 'Erica Ningroem Sarie', 'erikaens21', '$2y$10$h2bExa0pJnIYV0HBHCunOOd9tIxHpNKuIPRsben5eJEi5aPd7zjRK', 'e27b2ee833568de392ae55cfc90aea90c9031aac', 1, '2017-08-08 08:03:52', 3, 4, '21bb038e99d3b4ccf046cfcd0d573233ad4377db'),
(5, 'Isyraqi Putra', 'isyraqi', '$2y$10$R6JOmaQz9wZCv1L.K74z4ex4jWDVaZi8qq.4FAUvCVD.e/cooK2na', '63201cc2461c6497cb97b03a75da3e6383d9a50b', 1, '2017-08-08 10:13:22', 3, 1, 'd3b0edfc951ca57b790097ae8e5d03636a1c8661');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_info_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_lahir_tempat` varchar(20) DEFAULT NULL,
  `user_lahir_tanggal` date DEFAULT NULL,
  `user_jenis_kelamin` enum('1','2') DEFAULT NULL,
  `user_agama` enum('1','2','3','4','5') DEFAULT NULL,
  `user_status_perkawinan` int(1) DEFAULT NULL,
  `user_pendidikan` varchar(20) DEFAULT NULL,
  `user_kontak` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_info_id`, `user_id`, `user_lahir_tempat`, `user_lahir_tanggal`, `user_jenis_kelamin`, `user_agama`, `user_status_perkawinan`, `user_pendidikan`, `user_kontak`) VALUES
(4, 2, 'Tangerang', '1997-04-27', '1', '1', 2, 'Informatika', '085694325922'),
(7, 5, '', '0000-00-00', '1', '4', 2, '', '1234567'),
(8, 4, '', '0000-00-00', NULL, '1', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_role_name`) VALUES
(1, 'Perangkat Kecamatan'),
(2, 'Perangkat Desa'),
(3, 'Warga');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apbd`
--
ALTER TABLE `apbd`
  ADD PRIMARY KEY (`apbd_id`),
  ADD KEY `apbd_createdby` (`apbd_createdby`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`desa_id`),
  ADD UNIQUE KEY `desa_url` (`desa_url`),
  ADD UNIQUE KEY `desa_key` (`desa_key`),
  ADD KEY `desa_createdby` (`desa_createdby`);

--
-- Indexes for table `desa_jabatan`
--
ALTER TABLE `desa_jabatan`
  ADD PRIMARY KEY (`desa_jabatan_id`),
  ADD KEY `desa_id` (`desa_id`);

--
-- Indexes for table `desa_perangkat`
--
ALTER TABLE `desa_perangkat`
  ADD PRIMARY KEY (`desa_perangkat_id`),
  ADD KEY `desa_id` (`desa_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`),
  ADD KEY `history_changeby_id` (`history_changeby_id`);

--
-- Indexes for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD PRIMARY KEY (`keuangan_id`),
  ADD KEY `apbd_id` (`apbd_id`),
  ADD KEY `desa_id` (`desa_id`),
  ADD KEY `keuangan_createdby` (`keuangan_createdby`);

--
-- Indexes for table `keuangan_lain`
--
ALTER TABLE `keuangan_lain`
  ADD PRIMARY KEY (`keuangan_lain_id`),
  ADD KEY `desa_id` (`desa_id`),
  ADD KEY `keuangan_createdby` (`keuangan_lain_createdby`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`produk_url`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `produk_gambar`
--
ALTER TABLE `produk_gambar`
  ADD PRIMARY KEY (`produk_gambar_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_username` (`user_username`),
  ADD UNIQUE KEY `user_key` (`user_key`),
  ADD KEY `user_createdby` (`user_createdby`),
  ADD KEY `user_role_id` (`user_role_id`),
  ADD KEY `user_desa_id` (`user_desa_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_info_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apbd`
--
ALTER TABLE `apbd`
  MODIFY `apbd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
  MODIFY `desa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `desa_jabatan`
--
ALTER TABLE `desa_jabatan`
  MODIFY `desa_jabatan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `desa_perangkat`
--
ALTER TABLE `desa_perangkat`
  MODIFY `desa_perangkat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keuangan`
--
ALTER TABLE `keuangan`
  MODIFY `keuangan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `keuangan_lain`
--
ALTER TABLE `keuangan_lain`
  MODIFY `keuangan_lain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `produk_gambar`
--
ALTER TABLE `produk_gambar`
  MODIFY `produk_gambar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `apbd`
--
ALTER TABLE `apbd`
  ADD CONSTRAINT `apbd_ibfk_1` FOREIGN KEY (`apbd_createdby`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_ibfk_1` FOREIGN KEY (`desa_createdby`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `desa_jabatan`
--
ALTER TABLE `desa_jabatan`
  ADD CONSTRAINT `desa_jabatan_ibfk_1` FOREIGN KEY (`desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `desa_perangkat`
--
ALTER TABLE `desa_perangkat`
  ADD CONSTRAINT `desa_perangkat_ibfk_1` FOREIGN KEY (`desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `desa_perangkat_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD CONSTRAINT `keuangan_ibfk_1` FOREIGN KEY (`apbd_id`) REFERENCES `apbd` (`apbd_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `keuangan_ibfk_2` FOREIGN KEY (`desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `keuangan_ibfk_3` FOREIGN KEY (`keuangan_createdby`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `keuangan_lain`
--
ALTER TABLE `keuangan_lain`
  ADD CONSTRAINT `keuangan_lain_ibfk_1` FOREIGN KEY (`desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `keuangan_lain_ibfk_2` FOREIGN KEY (`keuangan_lain_createdby`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produk_gambar`
--
ALTER TABLE `produk_gambar`
  ADD CONSTRAINT `produk_gambar_ibfk_1` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`user_role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`user_desa_id`) REFERENCES `desa` (`desa_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
