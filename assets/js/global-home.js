$(document).ready(function(){
  $('.datatables').DataTable();
});

function toRupiah(isi = 0){
  isi = parseInt(isi);
  return 'Rp '+isi.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}

function desa_select_tahun_anggaran(elem){
  var tahun = $(elem).val();
  var desa_id = $('input[name="desa_id"]').val();

  $.ajax({
    url: base_url('home/pemakaian_json'),
    type: 'POST',
    data: {desa_id:desa_id,tahun:tahun},
    success: function(data){
      $('.table-ubah').html(data);
    }
  });
}

// function desa_select_tahun_anggaran(elem){
//   var tahun = $(elem).val();
//   var desa_id = $('input[name="desa_id"]').val();
//   $('.table.table-anggaran tbody').html('');
//
//   $.ajax({
//     url: base_url('home/jsonGetAnggaran/'+tahun+'/'+desa_id),
//     type: 'GET',
//     dataType: 'json',
//     success: function(data){
//       var no = 1;
//       var sisa = 0;
//       var tr = "";
//       $.each(data.apbd.apbd, function(index, element){
//         tr += "<tr>";
//         tr += "<td>"+no+"</td>";
//         tr += "<td>"+element.apbd+"</td>";
//         tr += "<td>-</td>";
//         tr += "<td>-</td>";
//         tr += "<td style='text-align:right;'>"+toRupiah(element.res.keuangan_anggaran)+"</td>";
//         tr += "</tr>";
//         sisa += isNaN(parseInt(element.res.keuangan_anggaran)) ? 0 : parseInt(element.res.keuangan_anggaran);
//         no++;
//       });
//       $.each(data.apbd.keuangan, function(index, element){
//         tr += "<tr>";
//         tr += "<td>"+no+"</td>";
//         tr += "<td>"+element.keuangan_lain_uraian+"</td>";
//         tr += "<td>-</td>";
//         tr += "<td>-</td>";
//         tr += "<td style='text-align:right;'>"+toRupiah(element.keuangan_lain_anggaran)+"</td>";
//         tr += "</tr>";
//         sisa += parseInt(element.keuangan_lain_anggaran);
//         no++;
//       });
//       $('.table.table-anggaran tbody').append(tr);
//
//       $('.table.table-anggaran tbody').append("<tr><td colspan='4'><b>Jumlah</b></td><td style='text-align:right;font-weight:bold;'>"+toRupiah(sisa)+"</td></tr>");
//
//     }
//   });
// }
