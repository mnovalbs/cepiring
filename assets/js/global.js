var perangkat_pertama = "";
var thumb_pertama     = "";
var byk_perangkat     = 1;
var byk_thumb         = 1;


$(document).ready(function(){

  $('.select2').select2();

  $('.dtables').DataTable();
  $(".perangkat-pertama").ready(function(){
    perangkat_pertama  = $(".perangkat-pertama").html();
    byk_perangkat      = $(".perangkat-input").length;
  });

  $(".thumb-pertama").ready(function(){
    thumb_pertama      = $(".thumb-pertama").html();
    byk_thumb          = $(".thumbnails .kotak").length;
  });

});

$(".add-box .kotak").click(function(){
  var thumb_baru  = thumb_pertama.replace(/\[0\]/g, '['+byk_thumb+']');
  thumb_baru      = thumb_baru.replace(/0/g, byk_thumb);

  $(".thumbnails").append("<div class='kotak'>"+thumb_baru+"</div>");
  byk_thumb++;
});

function gantiGambar(elem){
  if( elem.files && elem.files[0] ){
    var reader = new FileReader();
    reader.onload = function(e){
      $(elem).parents('.kotak').css('background-image', 'url('+e.target.result+')');
    }
    reader.readAsDataURL(elem.files[0]);
  }
}

function gantiThumb(elem){
  if( elem.files && elem.files[0] ){
    var reader = new FileReader();
    reader.onload = function(e){
      $(elem).parents('.desa-thumb').css('background-image', 'url('+e.target.result+')');
    }
    reader.readAsDataURL(elem.files[0]);
  }
};

function hapusGambar(elem, produk_id = 0){
  $(elem).parents('.kotak').remove();
  $(".gambar_hapus").find('option[value='+produk_id+']').attr('selected', true);
}

function tambah_perangkat(){
  var perangkat_baru = perangkat_pertama.replace(/\[0\]/g, '['+byk_perangkat+']');
  $(".jabatan-box").append("<div class='form-group perangkat baru'>"+perangkat_baru+"</div>");
  $(".baru").find('input').val('');
  $('.baru').find('select option:selected').removeAttr('selected');
  byk_perangkat++;
}

function perangkat_delete(elem){
  $(elem).parents('.form-group').remove();
}

tinymce.init({
  selector: '.tinymce',
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste jbimages"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
  height: 300,
});

function ubahAPBD(elem){
  var apbd_nama = $(elem).parents('tr').find('.apbd_nama').text();
  var apbd_id   = $(elem).parents('tr').data('id');
  var apbd_kategori = $(elem).parents('tr').find('.apbd_kategori span').data('id');
  apbd_kategori = parseInt(apbd_kategori);

  if( apbd_kategori == 1 ){
    $(elem).parents('tr').find('.apbd_kategori').html("<select name='apbd_kategori' class='form-control'><option selected value='1'>Bantuan Keuangan</option><option value='2'>PAD</option></select>");
  }else{
    $(elem).parents('tr').find('.apbd_kategori').html("<select name='apbd_kategori' class='form-control'><option value='1'>Bantuan Keuangan</option><option selected value='2'>PAD</option></select>");
  }

  $(elem).parents('tr').find('.apbd_nama').html("<input class='form-control' required name='apbd_name' maxlength='40' value='"+apbd_nama+"'/><input type='hidden' name='apbd_id' value='"+apbd_id+"'/>");
  $(elem).parents('tr').find('.tombol').html("<button onclick='doUbahAPBD(this)' type='button' name='ubah_apbd' class='btn btn-primary'><i class='fa fa-pencil'></i> Ubah</button>");

  var elem_baru = $(elem).parents('tr').html();
  $(elem).parents('tr').html( elem_baru );
}

function doUbahAPBD(elem){
  var apbd_nama = $(elem).parents('tr').find('input[name="apbd_name"]').val();
  var apbd_id   = $(elem).parents('tr').find('input[name="apbd_id"]').val();
  var apbd_kategori = $(elem).parents('tr').find('select[name="apbd_kategori"]').val();

  $(elem).html("<i class='fa fa-con'></i> Loading...");

  $.ajax({
    url: location.href,
    data: {apbd_nama:apbd_nama, apbd_id:apbd_id, apbd_kategori:apbd_kategori},
    type: 'POST',
    success: function(){

    },
    error: function(){
      alert("Error!");
    },
    complete: function(){
      location.reload();
    }
  });
}

var rowKeuanganPertama  = "";
var bykRowKeuangan      = 0;
$('.keuangan-row-pertama').ready(function(){
  bykRowKeuangan      = parseInt( $('.keuangan-row-pertama').find('td:first-child').text() );
  rowKeuanganPertama  = $('.keuangan-row-pertama').html();
});

function tambahRowKeuangan(){
  var repRow  = new RegExp(bykRowKeuangan, "g");
  var tmbhrow = $('.tambah-row').html();
  rowKeuanganPertama = rowKeuanganPertama.replace(repRow, bykRowKeuangan+1);
  $('.tambah-row').remove();
  $('.table').append("<tr>"+rowKeuanganPertama+"</tr>").append("<tr class='tambah-row'>"+tmbhrow+"</tr>");
  bykRowKeuangan++;
}

function get_bulan(num){
  switch(num){
    case 1 : {
      return 'Januari';
      break;
    }
    case 2 : {
      return 'Februari';
      break;
    }
    case 3 : {
      return 'Maret';
      break;
    }
    case 4 : {
      return 'April';
      break;
    }
    case 5 : {
      return 'Mei';
      break;
    }
    case 6 : {
      return 'Juni';
      break;
    }
    case 7 : {
      return 'Juli';
      break;
    }
    case 8 : {
      return 'Agustus';
      break;
    }
    case 9 : {
      return 'September';
      break;
    }
    case 10 : {
      return 'Oktober';
      break;
    }
    case 11 : {
      return 'November';
      break;
    }
    case 12 : {
      return 'Desember';
      break;
    }
  }
}

function select_bulan(name = '', num = 0){
  var sel = "<select class='form-control' name='"+name+"'>";
  sel += "<option value=''>Bulan</option>";
  for(i=1;i<=12;i++){
    if(num==i){
      sel += "<option selected value='"+i+"'>"+get_bulan(i)+"</option>";
    }else{
      sel += "<option value='"+i+"'>"+get_bulan(i)+"</option>";
    }
  }
  sel += "</select>";
  return sel;
}

function keuanganGantiTahun(){
  var tahun   = $("select[name='keuangan_tahun']").val();
  var desa_id = 0;
  if( $("select[name='keuangan_desa']").length != 0 ){
    desa_id = $("select[name='keuangan_desa']").val();
  }

  $('tbody').html('');

  $.ajax({
    url: base_url('keuangan/json/'+tahun),
    type: 'GET',
    data: {desa_id:desa_id},
    dataType: 'json',
    success: function(data){
      var i = 0;
      $(".angka-apbd").html('');
      var tr  = "<tr class='angka-apbd'>";
      var total = 0;
      $.each(data.apbd, function(index, element){
        $.each(element, function(a,b){
          tr += "<td>";
          tr += "<input class='form-control' type='number' name='apbd["+b.id+"]' value='"+b.res+"' />";
          tr += "</td>";
          total += parseInt(b.res);
          i++;
        });
      });
      tr += "</tr>";
      $('tbody').append(tr);
      $('tbody').append("<tr><td><b>Total : </b></td><td colspan='"+(i-1)+"' style='text-align:right;'><b>"+toRupiah(total)+"</b></td></tr>")
    }
  });
}

$('.rp_apbd').change(function(){
  var total = 0;
  $('.rp_apbd').each(function(){
    var isi = $(this).val();
    isi = parseInt(isi);
    total += isi;
  });
  $('.jumlah_apbd').text( toRupiah(total) );
});

function toRupiah(isi = 0){
  isi = parseInt(isi);
  return 'Rp '+isi.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}

function editBidang(elem, kb_id){
  var nama_bidang = $(elem).parents('tr').find('.nama-bidang').text();
  var inp = "<input class='form-control' value='"+nama_bidang+"' name='nama_bidang'/>";
  $(elem).parents('tr').find('.nama-bidang').html(inp);
  $(elem).parents('td').html("<button type='button' class='btn btn-primary' onclick='doUbahBidang(this,"+kb_id+")'><i class='fa fa-pencil'></i> Ubah</button>");
}

function doUbahBidang(elem, kb_id){
  var nama_bidang = $(elem).parents('tr').find("input[name='nama_bidang']").val();
  $(elem).html("<i class='fa fa-cog fa-spin'></i> Loading...");
  $.ajax({
    url: base_url('apbd/bidang/edit/'+kb_id),
    type: 'POST',
    timeout: 20,
    dataType: 'json',
    data: {nama_bidang:nama_bidang},
    success: function(data){
      if( data.res ){

      }else{
        alert("Gagal mengubah bidang!");
      }
    },
    complete: function(){
      location.reload();
    }
  });
}

function keuanganChangeBidang(elem){
  var bidang_id = $(elem).val();
  $("#kegiatan_select").html("<option value=''>Loading...</option>");
  $.ajax({
    url: base_url('keuangan/jsonKegiatan/'+bidang_id),
    type: 'GET',
    timeout: 20000,
    dataType: 'json',
    success: function(data){
      $("#kegiatan_select").html("");
      $.each(data, function(index, element){
        $("#kegiatan_select").append("<option value='"+element.kk_id+"'>["+element.kk_kode+"] "+element.kk_uraian+"</option>");
      });
    }
  });
}

function tambahSumber(elem, kegiatan_num, sumber_num){
  var sumber_sekarang = $(elem).parents('.row').html();
  sumber_baru = sumber_sekarang.replace(/sumber\]\[(.+?)\]/g, "sumber]["+(sumber_num+1)+"]");
  sumber_baru = sumber_baru.replace(/tambahSumber\((.+?)\)/g, "tambahSumber(this, "+kegiatan_num+", "+(sumber_num+1)+")");
  sumber_baru = sumber_baru.replace(/selected/g, "");
  $(elem).parents('.row').find('.tombol-hapus').html("<button type='button' class='btn btn-danger' onclick='hapusSumber(this)'><i class='fa fa-trash'></i> Hapus</button>");
  $(elem).parents('.kotak-sumber').append("<div class='row'>"+sumber_baru+"</div>");
  $(elem).remove();

}

var kegiatan_pertama = "";
$(".kegiatan-pertama").ready(function(){
  kegiatan_pertama  = $(".kegiatan-pertama").html();
});

function tambah_kegiatan(elem, num){
  kegiatan_baru = kegiatan_pertama.replace(/kegiatan\[(.+?)\]/g, "kegiatan["+(num+1)+"]");
  $(".kotak-kegiatan-tambah").append("<div class='kegiatan'>"+kegiatan_baru+"</div>");
  var elem_lama = $(elem).html();
  // var elem_baru = elem_lama.replace(/tambah_kegiatan\((.+?)\)/g, "tambah_kegiatan(this, "+(num+1)+")");
  $(elem).attr('onclick', "tambah_kegiatan(this, "+(num+1)+")");
  // $(elem).html(elem_baru);
}

function hapusSumber(elem){
  $(elem).parents('.row').remove();
}

function ubahTahunPemakaian(){
  var tahun = $("select[name='pemakaian_tahun']").val();
  var desa_id = 0;

  if( $("select[name='pemakaian_desa']").length != 0 ){
    desa_id = $("select[name='pemakaian_desa']").val();
  }

  $.ajax({
    url: base_url('keuangan/pemakaian/json/'+tahun),
    type: 'GET',
    data: {desa_id:desa_id},
    success: function(data){
      $('.table-ubah').html(data);
    }
  });
}
